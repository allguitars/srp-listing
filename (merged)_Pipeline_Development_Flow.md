
# Developing Pipelines in WISE-PaaS

The document is focused on the specifications of developing the pipelines for SRP listing process.

## 1. Development Environment

### 1.1 GitLab

Each SRP team will have its own project named as the SRP. The developers in charge in the SRP will have access to the project.

- GitLab Group: WISE-PaaS_SRP_Pipeline
- GitLab Project: the same as the SRP's name
- GitLab development branch: `develop` branch
- `feature-xxx` branch: The pipeline under development by the SRP dev team

![](media/Untitled-24a4e29f-525d-4774-b89c-00f946d680fb.png)

### 1.2 Blob

Each SRP Team will get their own Blob storage for development and the corresponding access key.

### 1.2 Jenkins

- Log in [here](http://jenkins-dev.eastasia.cloudapp.azure.com/) for the independent development environment with [Jenkins](https://jenkins.io/).
- Each SRP will be given two accounts:
    - One that can create, edit, and delete pipelines
    - One that can only execute pipelines, allowing others to run the pipeline under development
- The account will have a prefix as the name of the SRP. The prefix is not case-sensitive.

![](media/Untitled-26879e2f-8e59-4836-b322-ce43d0233cf5.png)

### 1.4 Apollo

- Log in [here](http://portal-apollo-dev.eastasia.cloudapp.azure.com/) for the individual development environment with Apollo.
- All Information for SRP listing have to be filled out. Please refer to the [SRP Config Spec](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/SRPConfigSpecification.md).
- Each SRP will have an account for editing the SRP listing configuration.

![](media/Untitled-277f5581-3b06-4a5d-9595-a5e768ad70be.png)

## 2. Development Process

![](media/Untitled-6820a88b-2fab-4c52-830f-84463feef0c4.png)

- **Step 1**: Upload your SRP package to the Blob storage.
- **Step 2**: In Apollo, submit and deploy the configurations with SRP's **Namespace**. Refer to the [SRP Config Spec](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/SRPConfigSpecification.md).

    ![](media/Untitled-ae1a8ac2-fc1e-4ecb-8754-3b58d2259cb9.png)

- **Step 3**: Within GitLab, SRP team will **pull** the *feature-xxx* branch from the *develop* branch to edit the pipeline for the specific project.
- **Step 3.1**: Edit your pipeline so that it can be used to download the SRP package from the Blob storage and deploy it. For editing the pipeline, refer to:
    - [How to write a deployment pipeline](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/How%20to%20write%20deploy%20pipeline.md)
    - [Start editing your deployment pipeline with a template](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/deployAppTemplate.groovy)
- **Step 3.2**: Use [Listing Management API](http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/rmm) to retrieve the SRP configuration that has been deployed in Apollo.

    **Note**: The API can only retrieve the configuration that has been deployed. Provide the deployment name of your Apollo configurations when requesting the SRP listing.

    ![](media/Untitled-a2f1a2a0-60df-42fe-9686-1d3b81a1b647.png)

- **Step 3.3**: Test your pipeline in our [Jenkins](http://jenkins-dev.eastasia.cloudapp.azure.com/) portal while developing it. Note that the prefix of your pipeline's name can only be your SRP name as demonstrated in the following image.

    ![](media/Untitled-77864373-5b59-4953-8774-a42bb443511f.png)

 

- One the pipeline passes your test, merge the *feature-xxx* branch into the *develop* branch and tag it with `v-x.x.x`, usually starting with `1.0.0`.
- **Step 4**: Submit your pipeline for QA testing.