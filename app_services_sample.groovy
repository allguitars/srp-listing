def Deploydependsrps(app)
{
	if(app['name'] == "Grafana"){
		build job: 'Pipeline-Grafana-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'StringParameterValue', name: 'PrivatePluginsURL', value: "${PrivatePluginsURL}"],
			[$class: 'StringParameterValue', name: 'logo', value: "${logo}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'DeployAllPlugins', value: "${DeployAllPlugins}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	
	if(app['name'] == "SaaSComposer")
	{
		build job: 'Pipeline-SaaSComposer-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'cf_postgresql_schema', value: "${cf_postgresql_schema}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	if(app['name'] == "SCADA")
	{
		build job: 'Pipeline-SCADA-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'mongodb_service_info', value: "${mongodb_service_info}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'rabbitmq_service_info', value: "${rabbitmq_service_info}"],
			[$class: 'StringParameterValue', name: 'influxdb_service_info', value: "${influxdb_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfInfluxdb', value: "${IfInfluxdb}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	if(app['name'] == "RMM")
	{
		build job: 'Pipeline-RMM-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'mongodb_service_info', value: "${mongodb_service_info}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'rabbitmq_service_info', value: "${rabbitmq_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	if(app['name'] == "Notification"){
		build job: 'Pipeline-Notification-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'mongodb_service_info', value: "${mongodb_service_info}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'rabbitmq_service_info', value: "${rabbitmq_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	
}
