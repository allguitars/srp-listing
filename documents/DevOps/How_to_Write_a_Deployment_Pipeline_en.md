
# How to Write a Deployment Pipeline for Your SRP

The document is focused on following facets to introduce how to write and develop the SRP pipelines.
- [Pipeline development environment](#development-environment)
- [Pipeline development flow](#pipeline-development-flow)
- [Creating a pipeline from a template](#creating-a-pipeline-from-a-template)
- [Test your pipeline](#test-your-pipeline)

<br>

## Development Environment

This section introduces the tools used during the pipeline development.

### 1. GitLab

Each SRP team will have its own project named as the SRP. The developers in charge in the SRP will have access to the project. Once the pipeline script has been completed, **commit** it to the `develop` branch of your pipeline project and **tag** it as `v-x.x.x.x`.

- GitLab Group: You should have access to the [WISE-PaaS_SRP_Pipeline](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline) group.
- GitLab SRP Pipeline Project: It is located in the [WISE-PaaS_SRP_Pipeline](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline) group and has the same name as the SRP itself, such as RMM. This project is to store the pipeline scripts for the SRP.
- Branches in the pipeline project
    - `develop` branch: The **README** file in this branch should contain the **usernames** and **passwords** for the SRP's Blob (for storing build packages), Apollo (for managing configurations), and Jenkins portal (for running pipelines).
    - `feature-xxx` branch: It is only for the SRP team's own development. Once done with developing the pipelines, they should merge it to the `develop` branch, commit it and give it a tag as mentioned above.
- **DevPublic** project: Contains the public scripts that will be used during the development, such as _ImportDashboard_ and _blobUploadDownload_. These scripts are read-only.
- **Documents** project: Documentation ready for the development.

### 2. Blob

Each SRP Team will get their own Blob storage for development and the corresponding access key.

### 3. Jenkins

- Log in [here](http://jenkins-dev.eastasia.cloudapp.azure.com/) for the independent development environment in **Jenkins**.
- Each SRP will be given two accounts:
    - One that can create, edit, and delete pipelines
    - One that can only run pipelines, allowing others to try the pipelines under development
- These accounts will have a prefix as the name of the SRP. The prefix is not case-sensitive.

### 4. Apollo

- Whenever done developing a new version of the product, the SRP dev team needs to fill out all information for that version in the **Apollo** configuration center so that the pipelines in **Jenkins** can retrieve the information using the API of the SRP listing system.
- Log in [here](http://portal-apollo-dev.eastasia.cloudapp.azure.com/) for the individual development environment in Apollo.
- All Information for SRP listing have to be filled out. Please refer to the [SRP Config Spec](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/SRPConfigSpecification.md).
- Each SRP will have an account for editing the SRP listing configuration in **Apollo**.

Snapshot in Apollo:

  ![](../uploads/images/DevOps/apollo-en.png)

<br>

## Pipeline Development Flow

![](../uploads/images/DevOps/pipeline-development-flow.png)

- **Step 1**: Upload your SRP package to the Blob storage if necessary.

- **Step 2**: In Apollo, submit and deploy configurations in the SRP's corresponding **Namespace**. For details about the configurations, see [SRP Config Spec](http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/Documents/blob/master/SRPConfigSpecification.md).

    ![](../uploads/images/DevOps/apollo-namespace.png)

- **Step 3**: In the **Pipeline project** within GitLab, SRP team needs to create a branch called `feature-xxx` from the `develop` branch for implementing the pipelines.

- **Step 3.1**: Edit your pipeline so that it can download the SRP package from the Blob storage and deploy it. For how to implement the pipelines, see the [How to write a deployment pipeline](#how-to-write-a-deployment-pipeline) section. You can also find the [pipeline template](#creating-a-pipeline-from-a-template) for creating your own pipeline.

- **Step 3.2**: Use [Listing Management API](http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/rmm) to retrieve the SRP's configuration that has been deployed in Apollo. **Note**: The API can only retrieve the configuration that has been **deployed**.

    ![](../uploads/images/DevOps/listing-management-api.png)

- **Step 3.3**: Test your pipeline in our [Jenkins portal](http://jenkins-dev.eastasia.cloudapp.azure.com/) while developing it. Note that you should use your *SRPName* for the prefix of the pipeline as demonstrated in the following image. For how to test your pipeline, see [Pipeline Test Flow](to be defined). // Todo

- **Step 4**: Once your pipeline passes the test, merge the `feature-xxx` branch back to the `develop` branch and tag it `v-x.x.x.x` (four digits), usually starting with `v-1.0.0.0`. Then submit your pipeline to QA for testing.

<br>

## How to Write a Deployment Pipeline

- Prepare Stage
- Deployment Stage
- Starting Stage
- Check the Deployment Result
- Rollback
- Set App's Hostname
- Blue-Green Deployment
- Smoke Test
- Exception Handling (Post mechanism)

### 1. PREPARE Stage

- Get the app configuration information from **Apollo**.

    ```groovy
        dir('json'){  
            if(Deployversion == "")
            {
                // Get the latest config information from Apollo through the listing system API and save it to a file called appInfo.json
                sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/srpname" -k > appInfo.json'
            }
            else
            {
            	// Get a specific version of config information from Apollo through the listing system API and save it to appInfo.json
            	sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/srpname?version=${Deployversion}" -k > appInfo.json' 
            }
        }

        // Get the python script needed for processing the JSON data
        dir('cf_operation'){
            retry(2){
                git credentialsId: "${git_credential}", url: 'http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/DevPublic.git'
            }
            sh 'chmod 770 *.sh'
        }

        // Process appInfo.json. The result will be saved to a file called apollo.json
        sh 'python3 ./cf_operation/parseJson.py ./json/appInfo.json'
        
        // Read apollo.json
        if(fileExists("apollo.json")){
            APPS = readJSON file: "apollo.json"
        }else{
            error "Connot get apollo config"
        }
    ```

- Initialize the config information.
    ```groovy
        // Convert cf_org and cf_space to lowercase for later use in manifest. These variables were given in Apollo config.
        def init(){
            cfOrgLow="${cf_org}".toLowerCase()
            cfSpaceLow="${cf_space}".toLowerCase()
            if(route_domain == ""){
                route_domain = "${cf_domain}"             
            }

            // Get the number of apps that will be deployed
            // An SRP can have multiple apps.
            appNums = APPS['param']['appNums']
        }
    ```

- Get `org_id` and `space_id`
    ```groovy
        sh "cf login -u $cf_username -p $cf_password -o $cf_org -s $cf_space -a api.$cf_domain --skip-ssl-validation;cf org $cf_org --guid>>orgid.txt;cf space $cf_space --guid>>spaceid.txt;"
        org_id=readFile('orgid.txt').trim()
        space_id=readFile('spaceid.txt').trim()
    ```

- Check if the service instances specified exist or not.
    ```groovy
        checkServiceExist(APPS)
    ```

- Check **ELK** status. **ELK** is a logging service. If this option is set to True in the Codepipeline, then the deployment process will also create this service.

    ```groovy
    if(IfELK=="true"){
        // Run the script to check username, password, domain, org, and space.
        // Then check if there is any ELK service instance in this space
        sh "../cf_operation/check_elk_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space}"
        
        // Get status from the text file generated from the previous step
        status=readFile("checkelk.txt").trim()
        echo status

        // if there is no ELK service instance in this space, then create it.
        if(status=="0"){
            sh "cf create-user-provided-service  ELK -l 'syslog://elk.eastasia.cloudapp.azure.com:5000'"
        }
    }
    ```

- Check if the app already exists in the space.
    ```groovy
        checkAppExist("beforeDeploy")
    ```

<br>

### 2. DEPLOYMENT Stage

- Define the services to be bound with your app.
    ```groovy
        // Determine the service names, plans, and serivce instances required by the app. 
        def defineService(appNames)
    ```

- Set up the **manifest** file
    ```groovy
        // MANIFEST_DIR to specify the path of each manifest file
        def setManifest(app,appZipName,serOrg,serviceMogo,servicePost,serviceRabbit,MANIFEST_DIR)
    ```

    - There are two steps in setting up the **manifest** file
        - First, retrieve the ZIP package from your Blob storage and unzip it.
            ```groovy
                appZipName="${app['sourceZipName']}_${app['version']}.zip"
                srpName = APPS['param']['srp_name']
                containerName = srpName.toLowerCase()

                // Modify this key with yours
                blob_container_key = "xxx"
                retry(3){
                    timeout(5){ 
                        // Download the specific version of zipped package
                        sh "python3 ../cf_operation/blobUploadDownload.py download ${containerName} ${app['sourceZipPath']}\\\\${appZipName}  ./${appZipName} ${blob_container_key}"
                    }
                }
                sh "unzip -o -d ./ ${appZipName}"
            ```

        - Second, modify your **manifest** file accordingly.
            ```groovy
                sh "cp manifest-production.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
                if(LICENSE=="true"){
                    sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['namePrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}" 
                }else{
                    sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['namePrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}" 
                }
            ```

- Deploy your app.
    ```groovy
        // Check if this app requires certain service. Here is an example
        // of postgresql service
        if(app.containsKey('postgresql_service_group')){
            servicePost = "${app['postgresql_service_name']}:${app['postgresql_service_plan']}:${app['postgresql_service_instance_name']}:${app['postgresql_service_group']}"
        }else{
            servicePost = null
        }
        
        retry(3){
            sh "./cf_operation/deploy_cf_service_nostart.sh api.${cf_domain} ${cf_username} ${cf_password} ${cf_org} ${cf_space} ${servicePost} ${serviceMogo} ${serviceRabbit} ${MANIFEST_DIR} manifest-${cfOrgLow}-${cfSpaceLow}.yml"
        }

        if(sso_url==""){
            sh "cf set-env ${app['name']}-${app['version']} sso_url ''"
        }else{
            sh "cf set-env ${app['name']}-${app['version']} sso_url ${sso_url}"
        }   
        
        // If app_seq is not defined, start all apps in parallel.
        if(null == app["app_seq"] || "" == app["app_seq"])
        {
            sh "cf start ${app['name']}-${app['version']}"
        }
    ```

<br>

### 3. STARTING Stage

- Start your apps in order according to the `app_seq`.
    ```groovy
        // If app_seq is defined.
        for(int i=0;i<appNums;i++){
            m = 0
            for(int j=0;j<appNums;j++){
                appM = ("app" + "${m}").toString()
                if(APPS.containsKey(appM) && APPS[appM]['status']=="0" && Integer.parseInt(APPS[appM]['app_seq']) == k){
                    appFullName = "${APPS[appM]['name']}-${APPS[appM]['version']}"
                    echo appFullName
                    retry
                    {
                        sh "cf start ${appFullName}"
                    }
                }
                m = m+1
            }
            k = k+1
        }
    ```
<br>

### 4. Check the Deployment Result

- Check if the app has been deployed successfully. The **status** should be *started*.
    ```groovy
        for(int i = 0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS.containsKey(appM) && APPS[appM]['status']=="0"){
                checkedApp = APPS[appM]
                checkedStatus = checkAppExist(checkedApp)
                APPS[appM]['checkStatus'] = checkedStatus
                if(checkedAppStatus == "0"){
                    rollStatus = true
                }
            }
            m = m+1
        }
        echo "checkApp ended..."
    ```
<br>

### 5. Roll-back

- If the **status** shows failed or not existed, then execute the **rollback** process. Then all apps that were just deployed will be deleted.
    ```groovy
        for(int i=0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            
            if(APPS.containsKey(appM)==true && APPS[appM] != null){
                if(APPS[appM]['status'] == "0"){
                    dApp = APPS[appM]['name']
                    dVersion = APPS[appM]['version']
                    dAppName = "${dApp}-${dVersion}"
                    echo dAppName
                    sh "cf delete ${dAppName} -r -f"
                }
            }
            m = m+1
        }
        echo "deploy app failed,it will roll back to previous version"
        return
    ```

<br>

### 6. Set App's Hostname

- `app_host` will be used to create the URL.
    ```groovy
        appNamePrefix = APPS[appM]['urlPrefix']
        if(appNamePrefix !=null && appNamePrefix!="")
        {
            Prefix = appNamePrefix.split(",")
            if(Prefix[0]!="" || Prefix[0]!=null)
            {   
                app_host=""
                for(int j = 0;j<Prefix.length;j++) // Multiple hosts
                {
                    if(j == Prefix.length-1)
                    {
                        app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"
                    }
                    else
                    {
                        app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"+","
                    }
                    //echo app_host >> host.txt
                    echo app_host
                }
            }   
        }
    ```

<br>

### 7. Smoke Test

- It is to make a request to an API of the app. If the response status is 200, then it will assume that this app is able to be visited normally.
    ```groovy
        for(int i=0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS[appM].containsKey("${appM}")){
                if(APPS[appM]['isFronted'] == "1"){
                    smokeTest("${APPS[appM]['name']}-${APPS[appM]['version']}")
                }
            }
            m = m+1
        }
    ```

<br>

### 8. Blue-Green Deployment
- Blue-green deployment will unbind the route from the old version and bind it to the new one. Then the new app will be started. 
- This stage always runs in **Parallel** as the following code shows.

    ```groovy
        parallel {
            stage("bluegreenswitch_app1"){
                steps{
                    script{
                        app00 = APPS["app0"]
                        bluegreenswitch(app00)
                    }
                }
            }
            stage('bluegreenswitch_app2'){
                steps{
                    script{
                        app01 = APPS['app1']
                        bluegreenswitch(app01)
                    }
                }
            }
            stage('bluegreenswitch_app3'){
                steps{
                    script{
                        app02 = APPS['app2']
                        bluegreenswitch(app02)
                    }
                }
            }
        }
    ```

<br>

### 9. Exception Handling (Post mechanism)

- **Failure**: If any of the above stages have problems, then we will enter the code block of Failure. This code block should execute the following three steps:
    1. Delete the app that was just deployed. There is no deletion if the app was not deployed.
    2. Go back to the previous version. If the previous version was not stopped, then there is no need for this step.
    3. Report the error message.
- **Success**: Enter this code block if each of the above stages has executed run successfully.

<br>

## Creating a Pipeline from a Template

The pipeline template is a ready-to-go script and can be re-used to create a customized pipeline. See the [template](Deployment_Pipeline_Template.md).

<br>

## Test Your Pipeline

- The SRP dev team needs to test their pipeline while developing in our [Jenkins](http://jenkins-dev.eastasia.cloudapp.azure.com/) portal.

- Pipeline's naming rule has to be followed. In Jenkins, the accounts can operate the SRP's pipelines that have the same prefix as the accounts, and this prefix will be the SRP's name. For example:

  - SRPName: *RMM*
  - Accounts: *rmm_edit*, *rmm_view*
  - **Pipeline's name: *rmm_deploy***

- The following image shows the page that you can add your own pipelines in Jenkins. The information needed to be set are:

  - GitLab repo URL
  - GitLab credential
  - `develop` branch
  - Pipeline name

  ![](../uploads/images/DevOps/jenkins-create-pipeline.png)

- Once all information are set up, you may start to build the pipeline with deployment parameters and test it.

