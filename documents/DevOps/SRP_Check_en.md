# SRP Listing Specifications

This document provides the specifications/requirements that the SRP needs to meet before it can be listed.

- **[MUST]** — the requirements that the SRP must fulfill for listing.
- **[RECOMMENDED]** — the requirements that our platform recommends the SRP to fulfill for listing.
- **[LEVEL-1]** — the requirements that the Listing Management Team will examine when the SRP listing request is issued.

# 1. SSO Integration

## 1.1 [LEVEL-1] SSO Log-in Page

- **[MUST]** Login page style should use WISE-PaaS CSS Template. Customized logo is permitted.
- [**RECOMMENDED**] SSO login page should include a link to the function to help the user recover the forgotten password.
- [**RECOMMENDED**] User’s name is identified and displayed after sign-in.

## 1.2 SRP Token Generation

- **[MUST]** SRP Token Format: base64Url_encode(AES_encode(timestamp-srpName))

    (timestamp is the time of the token generation)

## 1.3 [LEVEL-1] SSO URL

- **[MUST]** Support one of the following:
    - Make an user-provided environment variable for SSO URL, SRP uses the URL provided by the environment variable to interact with the SSO service.
    - No SSO URL environment variable. Instead, SRP determines which SSO URL to use base on the domain of the Org:
        1. WISE-PaaS Org: **https://portal-sso.${domain}**
        2. WISE-PaaS-Stage Org: **https://portal-sso-stage.${domain}**
        3. WISE-PaaS-Dev Org: **https://portal-sso-develop.${domain}**
        4. Other Orgs: **https://portal-sso.${domain}**

## 1.4 [LEVEL-1] SRP Default Privileges

- **[MUST]** SRP must support the following access control for the predefined roles:
    - **admin**: No requirements.
    - **tenant**: must be able to access SRP’s in the same org with the highest permission level without additional authorization.
    - **developer**: must be able to access SRP’s in the same org with the lowest permission level without additional authorization.
    - **srpUser**: must be able to access SRP once authorized.

## 1.5 [LEVEL-1] Use the Latest Version of SSO API

- **[MUST]** SRP should use the latest version of SSO API (currently 2.0) at the time of listing request.

## 1.6 Use Appropriate App ID

- [**RECOMMENDED**] Provide the Cloud Foundry app ID of the SRP when registering with the SSO service.
    - SRP Registration
      ```
      POST /v2.0/srps
      {
        "name": "",
        "orgId": "",
        "spaceId": "",
        "appId": "",
        "scopes": []
      }
      **Authentication
      SRPToken in HTTP header
      X-Auth-SRPToken:
      ```

    - SRP Upgrade

      ```
      PUT /v2.0/srps
      {
        "name": "",
        "orgId": "",
        "spaceId": "",
        "appId": "",
        "scopes": []
      }
      **Authentication
      SRPToken in HTTP header
      X-Auth-SRPToken:
      ```

## 1.7 Handling the Expired Token

- **[MUST]** Once signed in, app should use the refresh token API to obtain a new token package instead of using /auth/native API
- [**RECOMMENDED**] Token should be refreshed 10 minutes prior to token expiration.
- [**RECOMMENDED**] When to check for token validity:
    - If the application has auto logout feature, the application may check for token validity in the status check for auto logout.
    - If the application does not auto logout users, the application may use periodic checks for token validity.

## 1.8 [LEVEL-1] Cross-platform Support

- **[MUST]** Do not hardcode SSO URL. SRP’s may be deployed to different Orgs at different WISE-PaaS domains, each with a corresponding SSO URL.

# 2. Database

## 2.1 Reconnecting to the Service

- **[MUST]** In case of a connection failure between the application and platform services, the application should automatically reconnect once the services resume operation.
- [**RECOMMENDED**] Application should use delay algorithms, such as BEBA, for reconnection attempts to avoid creating unnecessary workload on the service.
- **[MUST]** Application should stop re-connection attempts if received account/password error.

## 2.2 Service Credentials

- **[MUST]** Allow the discovery of services to be based on the prefix of the service name. For example, MongoDB service name could be `mongodb` or `mongodb-shard` depending on the availability of the service.

## 2.3 Connection

- **[MUST]** Create and reuse connection pool. No more than 10 open connections per application.
- **[MUST]** Connections should be closed once the query is finished.
- [**RECOMMENDED**] Idle timeout set less than 10 minutes.
- [**MUST**] Support connection to a single node or cluster based on the connection string provided by the credential key or environment variable.

## 2.4 Creating the Index

- **[MUST]** Create appropriate index to improve query performance.
- **[MUST]** (PostgreSQL) If DB schema is altered as a result of an application upgrade, owner of the new schema should remain the same as the previous version.

# 3. IoT Hub

## 3.1 Publisher (Producer)

### 3.1.1 Re-connecting to the Service

- **[MUST]** In case of connection failure between the application and the IoT Hub, application should attempt to reconnect with a delay algorithm, such as BEBA, to avoid creating unnecessary workload on the IoT Hub.
- **[MUST]** Stop re-connection attempts if received account/password error.

### 3.1.2 Connection

- **[MUST]** Close unneeded connections.
- **[MUST]** If the producer (edge device) enters **sleep mode** after publishing data, make sure the connection is also closed.
- **[MUST]** Minimize the number of connection and connection attempts. Publishers should attempt to use the existing connection to send data.
- [**RECOMMENDED**] Support DCCS:
    - Obtain connection username and password with a credential key.
    - Query DCCS for service status first if data cannot be published to the IoT Hub.
    - Decide if application should try or stop reconnection attempt base on DCCS status code.
    - Use random delays between re-connection attempts.

### 3.1.3 Publishing Data

- [**RECOMMENDED**] Optimize data publish rate and message size.

### 3.1.4 High Availability

- [**RECOMMENDED**] Use **QoS 1** (ACK). (when performance is acceptable)
- [**RECOMMENDED**] When producer cannot publish data to the IoT Hub, the data should be kept locally until it can be published.

### 3.1.5 High Performance

- [**RECOMMENDED**] Use **QoS 0** (when there is no need for HA)

## 3.2 Subscriber (Consumer)

### 3.2.1 Credentials

- [**RECOMMENDED**] Use service binding to obtain connection credentials.

### 3.2.2 Connection

- **[MUST]** Minimize the number of connection and connection attempts. Consumers should attempt to use the existing connection to receive data.
- [**RECOMMENDED**] End points should use the maintain a client ID when reconnecting to IoT Hub.
- **[MUST]** Timeout should be set to be smaller than the keep alive time.

### 3.2.3 Re-connecting to the Service

- **[MUST]** Consumer should attempt to reconnect to the IoT Hub if link failure is detected.

### 3.2.4 High Availability

- **[MUST]** Use buffer mechanism to ensure consumers do not crash when processing a large number of queued messages after successful reconnection with the IoT Hub should there be a connection failure.
- [**RECOMMENDED**] Use durable queue and mirror queue. (when performance is acceptable)

### 3.2.5 High Performance

- [**RECOMMENDED**] Do not use durable queue. (when there is no need for HA)
- [**RECOMMENDED**] Use *auto-ack*. (when there is no need for HA)

# 4. Applications

## 4.1 12-factor Application

- [**RECOMMENDED**] Application follows the 12 design recommendation, known as the 12-Factors, for a software-as-a-service (SaaS) system. Combining 12-Factor design guidelines and the merits of Cloud Foundry to ensure the reliability and resilience of the applications.
- Reference: [https://portal-technical.wise-paas.com/doc/document-portal.html#CloudFoundryCli-2](https://portal-technical.wise-paas.com/doc/document-portal.html#CloudFoundryCli-2)

## 4.2 WISE-PaaS Buildpack Specification

- **[MUST]** Buildpacks provide framework and runtime support for apps. Buildpacks typically examine your apps to determine what dependencies to download and how to configure the apps to communicate with bound services.
- Please also refer to the documentation at [https://portal-technical.wise-paas.com/doc/document-portal.html#CloudFoundryCli-3](https://portal-technical.wise-paas.com/doc/document-portal.html#CloudFoundryCli-3)

## 4.3 [LEVEL-1] Manifest, App Name, & Routes

### 4.3.1 Manifest Specification

- **[MUST]** APP Manifest example:
  ```yaml
  applications:   
    - name: portal-javasamlecode-1.0.0       
      memory: 512M    
      disk_quota: 1024M    
      instances: 1 
      buildpack: Java_v3-19-2 
      health-check-type: http 
      services:      
      - mongodb      
      - rabbitmq
  ```

    - Note:
        1. Types of app health check include `port`, `process`, and `http`. Default is port, and usually, `http` health check is used for web service apps. Apps that functions as a background process should use the process health check type. See [details of the health check](https://docs.pivotal.io/pivotalcf/2-2/devguide/deploy-apps/healthchecks.html).
        2. Please refer to app naming policy. 
        3. App route will be set by the deployment pipeline during the deployment process to determine the Org and Space name needed to construct the host name.
        4. Please refer to section 4.2 for the usage guide on buildpacks.
        5. The services option can be omitted if the application is not using any service. In case of binding to PostgreSQL service, use the CF-CLI command tool instead of manifest option due to the fact that auto binding using manifest option does not support the use of binding parameters.
        6. Some of the environment variables in env are injected at the time of application deployment, i.e. org_name, org_id, mongodb_service_name, postgresql_service_name, rabbitmq_service_name, and so on. These variables need to be modifiable in the deployment pipeline script.

### 4.3.2 App Route Specification

- **[MUST]** Auto-deployable Apps (SRP) include the Org name and space name in the route to avoid conflict. For example, routes for apps in `AdvIIoT-EnE` org, `ECU` space:
    - **User Interface**

        Spec: *portal*-{OrgName}-{SpaceName}

        Example: portal-scada-adviiot-ene-ecu

    - **API Interface**

        Spec: *api*-{OrgName}-{SpaceName}

        Example: api-scada-adviiot-ene-ecu

    - **Document**

        Spec: *docs*-{OrgName}-{SpaceName}

        Example: docs-scada-adviiot-ene-ecu

    - **Dashboard**

        Spec: *dashboard*-{OrgName}-{SpaceName}

        Example: dashboard-scada-adviiot-ene-ecu

### 4.3.3 App Name Specification

- **[MUST]** App name is constructed with `app_type + project_name + version_number`. Usually, there are three segments to the version number, starting with 1.0.0.
    - **User Interface**

        Spec: *portal-*

        Example: portal-scada-1.0.0

    - **API Interface**

        Spec: *api-*

        Example: api-scada-1.0.0

    - **Background App**

        Spec: *-worker-*

        Example: scada-worker-1.0.0

    - **Document**

        Spec: *docs-*

        Example: docs-scada-1.0.0

    - **Dashboard**

        Spec: *dashboard-*

        Example: dashboard-scada-1.0.0

## 4.4 [LEVEL-1] Minimum Memory Quota

- **[MUST]** SRP must provide minimum application memory required for deployment.

# 5. Dashboard Integration

## 5.1 Managing Dashboard & SRP-Frame

- **[MUST]** Use dashboard Orgs to manage different SRPs to isolate user, DataSource, and Dashboards. During the deployment, allow the use of API to first create Dashboard Org with the name of the SRP, then import DataSource, Dashboards, and SRP-Frame configuration to the SRP’s Org using APIs.
- **[MUST]** SRP dashboards can be exported as JSON documents to be deployed with the API of the destined Dashboard app.
- **[MUST]** SRP-Frame configuration can be exported as a JSON document to be deployed with the API of the destined Dashboard app.

## 5.2 Using SRP-Frame

- **[MUST]** Use SRP-Frame function provided in the Dashboard. The stand-alone SRP-Frame is no longer maintained.
- **[MUST]** Configure the dashboard link in each SRP-Frame menu item to be the URL of its corresponding dashboard page.
- [**RECOMMENDED**] Avoid using `/dashboard/db/dashboard` slug to avoid dashboard loading problems. Suggest to use `/d/uid/dashboard` slug when defining the dashboard links for the SRP-Frame menu items. UID can be determined with the URL of the dashboard.

    An example for setting up the sidebar menu in SRP-Frame:
    ```json
    {
      "title": "SCADA",
      "type": "dashboard",
      "dashboard": "/d/DrmDnRwmk/plugin-upload"
    }
    ```