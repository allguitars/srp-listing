# Deployment Pipeline Template
This template can be re-used to create a customized pipeline.


```groovy
//初始化
def init(){
    cfOrgLow="${cf_org}".toLowerCase()
    cfSpaceLow="${cf_space}".toLowerCase()
    if(route_domain == ""){
        route_domain = "${cf_domain}"             
    }
    appNums = APPS['param']['appNums']
}
//检查app是否存在，checkedApp:app的名字，nameOfApp：app的prefix
def checkAppExist(check){
    if(check == "beforeDeploy"){
        m = 0
        for(int i=0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS.containsKey(appM)){
                nameApp = APPS[appM]['name']
                versionApp =  APPS[appM]['version']
                echo "${nameApp} version: ${versionApp}"
                checkedAppName = "${nameApp}-${versionApp}"
                sh " ./cf_operation/check_app_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space} ${checkedAppName}"
                checkedAppStatus=readFile("checkapp.txt").trim() 
                APPS[appM]['status'] = checkedAppStatus
                echo APPS[appM]['status']
                if(checkedAppStatus=="1"){
                    echo "${checkedAppName} exist!!!"
                }else{
                    echo "${checkedAppName} doesn't exist!!!"
                }
            }           
            m = m+1
        }
    }else{
        nameApp = check['name']
        versionOfApp = check['version']
        echo "${nameApp} version: ${versionOfApp}"
        checkedAppName = "${nameApp}-${versionOfApp}"
        sh " ./cf_operation/check_app_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space} ${checkedAppName}"
        checkedAppStatus=readFile("checkapp.txt").trim() 
        if(checkedAppStatus=="1"){
            echo "${checkedAppName} exist!!!"
        }else{
            echo "${checkedAppName} doesn't exist!!!"
        }
    }

    return checkedAppStatus
}
//检查Space中serviceinstance是否存在  
def checkServiceExist(APPS){
    isMongodbExist = false
    isPostgreExist = false
    isRapptiMqExist = false
    isInfluxdbExist = false
    com1 = '\'$1 ~ /^'
    com2 = '$/ {print $1}\''
    m = 0
    for(int i=0;i<appNums;i++){
        appM = ("app" + "${m}").toString()
        checkedApp = APPS[appM]
        if(checkedApp.containsKey('mongodb')&&isMongodbExist==false){
            if(mongodb_service_info == "")
            {
                mongodb = "mongodb"
            }
            else
            {
                mongodb_info_list = mongodb_service_info.split(':')
                mongodb = mongodb_info_list[2]
            }
            echo mongodb
            sh "cf services | grep ${mongodb} | awk ${com1}${mongodb}${com2}>>mm.txt"
            sh 'cat mm.txt' 
            status= readFile('mm.txt')
            status = status.trim()
            echo status
            if(status == mongodb){
                echo "mongodb instance exist"
            }
            else
            {
                error "mongodb instance not exist"
            }
            sh 'truncate -s 0 mm.txt'
        }
        if(checkedApp.containsKey('rabbitmq')&&isRapptiMqExist==false){
            if(rabbitmq_service_info == "")
            {
                rabbitmq = "rabbitmq"
            }
            else
            {
                rabbitmq_info_list = rabbitmq_service_info.split(':')
                rabbitmq = rabbitmq_info_list[2]
            }
            echo rabbitmq
            sh "cf services | grep ${rabbitmq} | awk ${com1}${rabbitmq}${com2}>>mm.txt"
            sh 'cat mm.txt' 
            status= readFile('mm.txt')
            status = status.trim()
            echo status
            if(status == rabbitmq){
                echo "rabbitmq instance exist"
            }
            else
            {
                error "rabbitmq instance not exist"
            }
            sh 'truncate -s 0 mm.txt'
        }
        if(checkedApp.containsKey('postgresql_service_group')&&isPostgreExist==false){
            if(postgresql_service_info == "")
            {
                postgresql = "postgresql"
            }
            else
            {
                postgresql_info_list = postgresql_service_info.split(':')
                postgresql = postgresql_info_list[2]
            }
            echo postgresql
            sh "cf services | grep ${postgresql} | awk ${com1}${postgresql}${com2} >>mm.txt"
            sh 'cat mm.txt' 
            status= readFile('mm.txt')
            status = status.trim()
            echo status
            if(status == postgresql){
                echo "postgresql instance exist"
            }
            else
            {
                error "postgresql instance not exist"
            }
            sh 'truncate -s 0 mm.txt'
        }
        if(checkedApp.containsKey('influxdb')&&isInfluxdbExist==false){
            if(influxdb_service_info == "")
            {
                influxdb = "influxdb"
            }
            else
            {
                influxdb_info_list = influxdb_service_info.split(':')
                influxdb = influxdb_info_list[2]
            }
            echo influxdb
            sh "cf services | grep ${influxdb} | awk ${com1}${influxdb}${com2} >>mm.txt"
            sh 'cat mm.txt' 
            status= readFile('mm.txt')
            status = status.trim()
            echo status
            if(status == influxdb){
                echo "influxdb instance exist"
            }
            else
            {
                error "influxdb instance not exist"
            }
            sh 'truncate -s 0 mm.txt'
        }
        m = m+1
    }
}
def defineService(appNames){
    if(appNames.containsKey('mongodb'))
    {

        if (mongodb_service_info == "") 
        {
            appNames['mongodb_service_name'] = "mongodb"
            appNames['mongodb_service_plan'] = "Shared"
            appNames['mongodb_service_instance_name'] = "mongodb"
        } 
        else
        {
            mongodb_info_list = mongodb_service_info.split(':')
            appNames['mongodb_service_name'] = mongodb_info_list[0]
            appNames['mongodb_service_plan'] = mongodb_info_list[1]
            appNames['mongodb_service_instance_name'] = mongodb_info_list[2]
        }
    }
    if(appNames.containsKey('rabbitmq'))
    {

        if (rabbitmq_service_info == "") 
        {
            appNames['rabbitmq_service_name'] = "p-rabbitmq"
            appNames['rabbitmq_service_plan'] = "standard"
            appNames['rabbitmq_service_instance_name'] = "rabbitmq"
        }
        else
        {
            rabbitmq_info_list = rabbitmq_service_info.split(':')
            appNames['rabbitmq_service_name'] = rabbitmq_info_list[0]
            appNames['rabbitmq_service_plan'] = rabbitmq_info_list[1]
            appNames['rabbitmq_service_instance_name'] = rabbitmq_info_list[2]
        }
    }
    if(appNames.containsKey('postgresql_service_group'))
    {

        if (postgresql_service_info == "") 
        {
            appNames['postgresql_service_name'] = "postgresql"
            appNames['postgresql_service_plan'] = "Shared"
            appNames['postgresql_service_instance_name'] = "postgresql"
        }
        else
        {
            postgresql_info_list = postgresql_service_info.split(':')
            appNames['postgresql_service_name'] = postgresql_info_list[0]
            appNames['postgresql_service_plan'] = postgresql_info_list[1]
            appNames['postgresql_service_instance_name'] = postgresql_info_list[2]
        }
    }
}
def setManifest(app,appZipName,serOrg,serviceMogo,servicePost,serviceRabbit,MANIFEST_DIR){
    dir(MANIFEST_DIR){
        appZipName="${app['sourceZipName']}_${app['version']}.zip"
        srpName = APPS['param']['srp_name']
        containerName = srpName.toLowerCase()
        blob_container_key = "xxx"
        retry(3){
            timeout(5){ 
                //下载指定版本的zip包
                sh "python3 ../cf_operation/blobUploadDownload.py download ${containerName} ${app['sourceZipPath']}\\\\${appZipName}  ./${appZipName} ${blob_container_key}"
            }
        }
        //解压zip包
        sh "unzip -o -d ./ ${appZipName}"
        if(app.containsKey('SERVER_ACTIVATENAME')){
            serOrg = "org_name:${cf_org},org_id:${org_id},SERVER_ACTIVATENAME:${app['SERVER_ACTIVATENAME']}"
        }else{
            serOrg = "org_name:${cf_org},org_id:${org_id}"
        }
        if(app.containsKey('mongodb')){
            serviceMogo = "${app['mongodb_service_name']}:${app['mongodb_service_instance_name']}"
        }else{  
            serviceMogo = null
        }
        if(app.containsKey('rabbitmq')){
            serviceRabbit = "${app['rabbitmq_service_name']}:${app['rabbitmq_service_instance_name']}"
        }else{
            serviceRabbit = null
        }
        if(app.containsKey('postgresql_service_group')){
            servicePost = "${app['postgresql_service_name']}:${app['postgresql_service_instance_name']}"
        }else{
            servicePost = null
        }
        if(app['urlPrefix'] != ""){
            sh "cp manifest-production.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
            if(LICENSE=="true"){
                sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['urlPrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}"  
            }else{
                sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['urlPrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}"  
            }
        }else{
            sh "cp manifest.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
            if(LICENSE=="true"){
                sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}"    
            }else{                                                                                                                                               
                sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}"    
            }
        }
        sh "cat manifest-${cfOrgLow}-${cfSpaceLow}.yml"
    }
}
def deployApp(app,serviceMogo,servicePost,serviceRabbit,MANIFEST_DIR){
    if(app.containsKey('mongodb')){
        serviceMogo = "${app['mongodb_service_name']}:${app['mongodb_service_plan']}:${app['mongodb_service_instance_name']}"
    }else{  
        serviceMogo = null
    }
    if(app.containsKey('rabbitmq')){
        serviceRabbit = "${app['rabbitmq_service_name']}:${app['rabbitmq_service_plan']}:${app['rabbitmq_service_instance_name']}"
    }else{
        serviceRabbit = null
    }
    if(app.containsKey('postgresql_service_group')){
        servicePost = "${app['postgresql_service_name']}:${app['postgresql_service_plan']}:${app['postgresql_service_instance_name']}:${app['postgresql_service_group']}"
    }else{
        servicePost = null
    }
    retry(3){
        sh "./cf_operation/deploy_cf_service_nostart.sh api.${cf_domain} ${cf_username} ${cf_password} ${cf_org} ${cf_space} ${servicePost} ${serviceMogo} ${serviceRabbit} ${MANIFEST_DIR} manifest-${cfOrgLow}-${cfSpaceLow}.yml"
    }
    if(sso_url==""){
        sh "cf set-env ${app['name']}-${app['version']} sso_url ''"
    }else{
        sh "cf set-env ${app['name']}-${app['version']} sso_url ${sso_url}"
    }           
    if(null == app["app_seq"] || "" == app["app_seq"]){
        sh "cf start ${app['name']}-${app['version']}"
    }
}
def bluegreenswitch(app){
    if(app['urlPrefix'] != ""){
        build job: 'pipeline_Blue_Green_Switch_t', parameters: [
            [$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
            [$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
            [$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
            [$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
            [$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
            [$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
            [$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
            [$class: 'StringParameterValue', name: 'app_name', value: "${app['name']}-${app['version']}"],
            [$class: 'StringParameterValue', name: 'app_host', value: "${app_host}"]]
    }else{
        build job: 'pipeline_Blue_Green_Switch_t', parameters: [
            [$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
            [$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
            [$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
            [$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
            [$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
            [$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
            [$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
            [$class: 'StringParameterValue', name: 'app_name', value: "${app['name']}-${app['version']}"],
            [$class: 'StringParameterValue', name: 'app_host', value: "NoRoute"]]
    }
}
//smoke Test,appName:要测试的app
def smokeTest(appName){
    com = '\'BEGIN {FS=" "}; {print $6 $7 $8 $9}\''
    sh "cf login -u ${cf_username} -p ${cf_password} -o ${cf_org} -s ${cf_space} -a api.${cf_domain} --skip-ssl-validation;cf target -o ${cf_org};cf target -s ${cf_space};cf apps | grep ${appName} | awk ${com} >>urls.txt"   
    sh 'cat urls.txt' 
    url=readFile("urls.txt")split(',')
    for(int i=0;i<url.length;i++){
        u = url[i].trim()
    if (u != "")
        {
            sh "curl -I -o /dev/null -s -w %{http_code} https://${u} >>ss.txt"
            stateCodeWithVersion=readFile('ss.txt')
            sh 'truncate -s 0 ss.txt'
            if(stateCodeWithVersion != "200"){
                error 'smoke test failed'
            }else{
                echo "smoke test success"
            }
        }
    }
    sh 'truncate -s 0 urls.txt'
    sh 'cat urls.txt'
}
pipeline{
    agent 
    {
        label 'jenkins-node-cf' 
    }
    parameters{
        credentials(name:'git_credential',defaultValue:'',description: '')
        string(name:'cf_username',defaultValue:'',description: '')  
        password(name:'cf_password',defaultValue:'',description: '')  
        string(name:'cf_org',defaultValue:'',description: '')  
        string(name:'cf_space',defaultValue:'',description: '') 
        string(name:'cf_domain',defaultValue:'',description: '') 
        string(name:'mongodb_service_info',defaultValue:'',description:'')
        string(name:'postgresql_service_info',defaultValue:'',description:'')
        string(name:'rabbitmq_service_info',defaultValue:'',description:'')
        booleanParam(name:'IfSmokeTest',defaultValue:false,description:'')  //IfSmokeTest：是否进行smoketest
        booleanParam(name:'IfMapUrlDirectly',defaultValue:true,description:'')  //IfMapUrlDirectly:是否进行url映射
        booleanParam(name:'IfRemainBlueVersion',defaultValue:false,description:'')//IfRemainBlueVersion：是否停用上一个版本，删除以前的版本
        string(name:'sso_url',defaultValue:'',description:'')
        string(name:'Deployversion',defaultValue:'',description:'')
        string(name:'mp_url',defaultValue:'',description:'')
        booleanParam(name:'IfELK',defaultValue:false,description:'')
        booleanParam(name:'LICENSE',defaultValue:true,description:'')
    }
    stages
    {
        stage('Prepare'){
            steps{
                script{
                    container('jenkins-node-cf'){
                        echo "Prepare start"
                        sh 'rm -rf *'
                        //参数定义
                        route_domain = ""
                        app_host = ""
                        APPS = ""
                        org_id = ""
                        space_id_md = ""
                        checkStatus = ""
                        rollStatus = "false"
                        appNums = ""
                        endFlag = 0                
                        //1.获取被解析apollo上的json 文件
                        dir('json')
                        {
                            //通过list system api获取最新的apollo配置信息到appInfo.json
                            if(Deployversion == "")
                            {
                                sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/srpname" -k > appInfo.json'
                            }
                            else
                            {   
                                //通过list system api获取指定版本的apollo配置信息到appInfo.json
                               sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/srpname?version=${Deployversion}" -k > appInfo.json' 
                            }
                        }
                        //获取python脚本用于处理从api获取到的json数据，并将处理结果存放到apollo.json
                        dir('cf_operation'){
                            retry(2){
                                git credentialsId: "${git_credential}", url: 'http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/DevPublic.git'
                            }
                            sh 'chmod 770 *.sh'
                        }
                        sh 'python3 ./cf_operation/parseJson.py ./json/appInfo.json'

                        //将apollo.json读取到APPS列表中
                        if(fileExists("apollo.json")){
                            APPS = readJSON file: "apollo.json"
                        }else{
                            error "Connot get apollo config"
                        }
                        //初始化
                        init()
                        sh "cf login -u $cf_username -p $cf_password -o $cf_org -s $cf_space -a api.$cf_domain --skip-ssl-validation;cf org $cf_org --guid>>orgid.txt;cf space $cf_space --guid>>spaceid.txt;"
                        sh "chmod 755 ~/.cf/config.json"
                        //3.登录cf获取到orgId和spaceId
                        org_id=readFile('orgid.txt').trim()
                        space_id=readFile('spaceid.txt').trim()
                        if(org_id=="FAILED"){
                            error "Get org_id failed!!!"
                        }
                        if(space_id=="FAILED"){
                            error "Get space_id failed!!!"
                        }else{
                            sh "echo -n ${space_id}|md5sum|cut -d ' ' -f1 > md5.txt"
                            space_id_md5 = readFile('md5.txt').trim()
                            echo "space_id_md5: ${space_id_md5}"
                        }
                        //检查服务
                        checkServiceExist(APPS)
                        //4.下载检查app是否存在的脚本
                        dir('cf_operation') {
                            if(IfELK=="true"){
                                //执行文本检查：通过用户名，密码，域，org和space
                                sh "../cf_operation/check_elk_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space}"
                                //读取文件
                                status=readFile("checkelk.txt").trim()
                                echo status
                                //状态值为0则创建日志
                                if(status=="0"){
                                    sh "cf create-user-provided-service  ELK -l 'syslog://elk.eastasia.cloudapp.azure.com:5000'"
                                }
                            }
                        }
                        //检查app是否已经部署
                        checkAppExist("beforeDeploy")
                        echo "Prepare Finished"
                    }
                }
            }
        }
        stage("deploy"){
            parallel{
                stage('app0'){
                    when{
                        expression{ 
                            return (APPS.containsKey("app0") && APPS['app0']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app00 = APPS['app0']
                                defineService(app00)
                                setManifest(app00,"app0Zipname","app0Org","mogo00","post00","rabbitmq00","app0")
                                deployApp(app00,"mogo00","post00","rabbitmq00","app0")
                            }
                        }
                    }
                }
                stage('app1'){
                    when{
                        expression{ 
                            return (APPS.containsKey("app1") && APPS['app1']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app01 = APPS['app1']
                                defineService(app01)
                                setManifest(app01,"app1Zipname","app1Org","mogo01","post01","rabbitmq01","app1")
                                deployApp(app01,"mogo01","post01","rabbitmq01","app1")
                            }
                        }
                    }
                }
                stage('app2'){
                    when{
                        expression{ 
                            return (APPS.containsKey("app2") && APPS['app2']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app02 = APPS['app2']
                                defineService(app02)
                                setManifest(app02,"app2Zipname","app2Org","mogo02","post02","rabbitmq02","app2")
                                deployApp(app02,"mogo02","post02","rabbitmq02","app2")
                            }
                        }
                    }
                }
            }
        }
        stage('sequenceStart'){
            when{
                expression{ 
                    //判断里面的App是否有启动顺序
                    return (APPS['app0']['app_seq'] != null)
                }
            }
            steps{
                script{
                    container('jenkins-node-cf'){
                        k = 1
                        for(int i=0;i<appNums;i++){
                            m = 0
                            for(int j=0;j<appNums;j++){
                                appM = ("app" + "${m}").toString()
                                if(APPS.containsKey(appM) && APPS[appM]['status']=="0" && Integer.parseInt(APPS[appM]['app_seq']) == k){
                                    appFullName = "${APPS[appM]['name']}-${APPS[appM]['version']}"
                                    echo appFullName
                                    sh "cf start ${appFullName}"
                                }
                                m = m+1
                            }
                            k = k+1
                        }
                    }                   
                }
            }
        }
        stage('checkApp'){
            steps{
                script{
                    container('jenkins-node-cf'){
                        echo "check app start"
                        m = 0;
                        for(int i = 0;i<appNums;i++){
                            appM = ("app" + "${m}").toString()
                            if(APPS.containsKey(appM) && APPS[appM]['status']=="0"){
                                checkedApp = APPS[appM]
                                checkedStatus = checkAppExist(checkedApp)
                                APPS[appM]['checkStatus'] = checkedStatus
                                if(checkedAppStatus == "0"){
                                    rollStatus = true
                                }
                            }
                            m = m+1
                        }
                        echo "checkApp end .............."
                    }
                }
            }
        }
        //8.如果有app未部署成功，那么回滚该所有app
        stage('rollBack'){
            when{
                expression{ 
                    return (rollStatus == true)
                }
            }
            steps{
                script{ 
                    container('jenkins-node-cf'){
                        m = 0;
                        for(int i=0;i<appNums;i++){
                            appM = ("app" + "${m}").toString()
                            if(APPS.containsKey(appM)==true && APPS[appM] != null){
                                if(APPS[appM]['status'] == "0"){
                                    dApp = APPS[appM]['name']
                                    dVersion = APPS[appM]['version']
                                    dAppName = "${dApp}-${dVersion}"
                                    echo dAppName
                                    sh "cf delete ${dAppName} -r -f"
                                }
                            }
                            m = m+1
                        }
                        endFlag = "1"
                        echo "deploy app failed,it's will roll back to last version"
                        error "Program execute failed"
                    }
                }
            }
        }
        stage('update-params'){
            steps{
                script{
                    container('jenkins-node-cf'){
                        m = 0
                        for(int i=0;i<appNums;i++){
                            appM = ("app" + "${m}").toString()
                            if(APPS[appM] == null || APPS[appM] == ""){
                                continue
                            }
                            if(APPS[appM]['urlPrefix'] != "")
                            {
                                appNamePrefix = APPS[appM]['urlPrefix']
                                if(appNamePrefix !=null && appNamePrefix!="")
                                {
                                    Prefix = appNamePrefix.split(",")
                                    if(Prefix[0]!="" || Prefix[0]!=null)
                                    {app_host=""
                                        for(int j = 0;j<Prefix.length;j++)
                                        {
                                            if(j == Prefix.length-1)
                                            {
                                                app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"
                                            }
                                            else
                                            {
                                                app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"+","
                                            }
                                            //echo app_host >> host.txt
                                            echo app_host
                                        }
                                    }   
                                }
                                echo app_host
                            }
                            m = m+1
                        }
                    }                       
                }
            }
        }
        //12.冒烟测试
        stage('smokeTest')
        {
            when{
                    expression{
                        return (IfSmokeTest =="true")
                    }
                }
            steps
            {
                script{
                    container('jenkins-node-cf'){
                        m = 0
                        for(int i=0;i<appNums;i++)
                        {
                            appM = ("app" + "${m}").toString()
                            if(APPS.containsKey(appM))
                            {
                                if(APPS[appM]['urlPrefix'] != "")
                                {
                                    smokeTest("${APPS[appM]['name']}-${APPS[appM]['version']}")
                                }
                            }
                            m = m+1
                        }
                    }
                }
            }
        }
        //10 蓝绿部署
        stage('bluegreenswitch_Stage') {
            parallel {
                stage("bluegreenswitch_app1"){
                    when{
                        expression{ 
                            return (APPS.containsKey("app0")&& APPS['app0']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app00 = APPS["app0"]
                                bluegreenswitch(app00)
                            }
                        }
                    }
                }
                stage('bluegreenswitch_app2'){
                    when{
                        expression{ 
                            return (APPS.containsKey("app1")&& APPS['app1']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app01 = APPS['app1']
                                bluegreenswitch(app01)
                            }
                        }
                    }
                }
                stage('bluegreenswitch_app3'){
                    when{
                        expression{ 
                            return (APPS.containsKey("app2")&& APPS['app2']['status']=="0")
                        }
                    }
                    steps{
                        script{
                            container('jenkins-node-cf'){
                                app02 = APPS['app2']
                                bluegreenswitch(app02)
                            }
                        }
                    }
                }
            }
        }
    }
    post 
    {           
        failure { 
            script {
                container('jenkins-node-cf'){
                    if(endFlag == 0){
                            //1.回滚
                        echo "rollBack start........."
                        m = 0;
                        for(int i=0;i<appNums;i++){
                            appM = ("app" + "${m}").toString()
                            if(APPS.containsKey(appM)==true && APPS[appM] != null){
                                if(APPS[appM]['status'] == "0"){
                                    dApp = APPS[appM]['name']
                                    dVersion = APPS[appM]['version']
                                    dAppName = "${dApp}-${dVersion}"
                                    echo dAppName
                                    sh "cf delete ${dAppName} -r -f"
                                }
                            }
                            m = m+1
                        }
                        echo "rollBack end........."
                        result = 'FAILURE'
                        echo "something wrong,all operation rollBack"
                    }
                }
            }

        }
        success{
            script{
                container('jenkins-node-cf'){
                    echo "SUCCUSS"
                }
            }
        }
    }
}
```