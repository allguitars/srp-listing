# !! Under Construction...

How to integrate with SSO
===

整合步骤
---

  * 1 前端页面

  * 2 在SSO注册SRP的信息以及SRP对于用户添加删除等整合SSO。
  
  * 3 SRP的权限验证整合SSO。
  
  * 4 常用情景参考。
  
  * 5 参考demo以及api doc地址
    

   注意：以下所提到的所有SSO的api,或者整合可能会用到的SSO api，都应使用SSO的最新版API。（目前为/v2.0）

前端页面
--- 

  * 1 登录页面的样式，必须满足整合规范的 [CSS Template](http://advgitlab.eastasia.cloudapp.azure.com/EI-PaaS-Common/Login-Template)
  
  * 2 建议登录页面支持忘记密码功能，忘记密码可调用SSO API。如下：

  ```
      GET /v2.0/users/{username}/pwdresetemail
      调用此接口，会给username对应的用户发送重置密码的邮件。
      
  ```

  * 3 建议登录页面提供记住用户名功能。

SRP注册
---

### SRP 如何获取 org guid, space guid 和 application guid

   * pipeline在部署时应修改manifest文件, 添加环境变量org_id。
     
   * 在将SRP部署到CF之后，其环境变量中就会存在space_id和application_id。
     
   * org_id 对应 org guid ,   space_id 对应 space guid ,  application_id 对应 application guid

### 如何生成SRPToken

   * base64Url_encode(AES_encode(timestamp-srpName)) (timestamp 为当前时间，10位时间戳)
    
   * AES_encode

   ``` 
       ○ AES/ECB/PKCS5Padding
       ○ Secret key: ssoisno12345678987654321
       ○ Golden unit test
       ■ Source of plain text: 1234567890-SCADA
       ■ base64Encode(AES encode(src)): (可利用 Online Crypto Tool 验证)
       ● h9mmu4CIc+YwBWDamtMKMA9tdDQNzz/RLTFfsfGoQhg=
       ■ base64UrlEncode(AES encode(src)): (实际带在 header里请用 base64UrlEncode)
       ● h9mmu4CIc-YwBWDamtMKMA9tdDQNzz_RLTFfsfGoQhg=
       ○ Online Crypto Tool (base64Encode)
       ■ https://goo.gl/6ig1No
       ○ Java Sample code
       ■ https://drive.google.com/open?id=0B0k7G85XKxSrVGRiNFNGa1c5b1k
       ○ Java Cryptography Extension issue:
       ■ http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html
   ```

### SRP向SSO注册

   * 1 SRP根据orgGuid、spaceGuid以及srpName从SSO获取SRP信息,如果获取不到当前SRP的注册信息，那么执行第二步。否则，直接执行第三步。

      ```
            SRP get SRP
            GET /v2.0/srps/{srpName}?orgId={orgGuid}&spaceId={spaceGuid}
            **Authentication
            SRPToken in HTTP header
            X-Auth-SRPToken: <SRPToken>
            
            Example:
               SRP get SRP
               GET
               /v2.0/srps/SCADA?orgId=df3bd89b-455b-46ee-b908-6eb8c17b8a5c&spaceId=8c86836c-a540-45d5-a8cd-5e9a5e69649d
               **Authentication
               SRPToken in HTTP header
               X-Auth-SRPToken: <SRPToken>
      ```

   * 2 SRP在SSO中创建SRP记录，也就是SRP在SSO进行注册。注册完成之后，执行第三步。

       ```
       SRP register
          POST /v2.0/srps
          {
           "name": "<string>",
           "orgId": "<string>",
           "spaceId": "<string>",
           "appId": "<string>",
           "scopes": []
          }
          name: SRP provide
          orgId: SRP provide org Guid
          spaceId: SRP provide space Guid
          appId: SRP provide application Guid
          scopes: SRP provide
          **Authentication
          SRPToken in HTTP header
          X-Auth-SRPToken: <SRPToken>
          
          Example:
              SRP register
              POST /v2.0/srps
              {
               "name": "SCADA",
               "orgId": "df3bd89b-455b-46ee-b908-6eb8c17b8a5c" //WISE-PaaS on Azure HK
               "spaceId": "8c86836c-a540-45d5-a8cd-5e9a5e69649d" //Develop
               "appId": "3c8692ca-b320-49d5-d8cd-5e9a5e6964ad" //application id
               "scopes": ["admin", "user"]
              }
              **Authentication
              SRPToken in HTTP header
              X-Auth-SRPToken: <SRPToken>
       ```

   * 3 到这里，SRP已经注册成功了, SRP应该自己保存SRP id以及SRP Secret ,以便别的功能使用。SRP id可以保存在内存或者数据库中（建议保存在数据库）。

### 当SRP升级应该在SSO上更新appid

   * 1 appid的作用：SSO会根据SRP的appid以及org和space等信息，查询到SRP的url。在sso的首页展示用户所有SRP的url,起到导航的作用。
  
   * 2 SRP升级如果appid变更就必须要向SSO进行同步，否则可能导致SSO不能显示正确的SRP的url。
  
   * 3 更新SRP的数据，参考如下接口。

     ```
     SRP update appId
        PUT /v2.0/srps
        {
         "name": "<string>",
         "orgId": "<string>",
         "spaceId": "<string>",
         "appId": "<string>",
         "scopes": []
        }
        name: SRP provide
        orgId: SRP provide org Guid
        spaceId: SRP provide space Guid
        appId: SRP provide application Guid
        scopes: SRP provide
        **Authentication
        SRPToken in HTTP header
        X-Auth-SRPToken: <SRPToken>
     ```

### 设置用户对于当前SRP的权限

   * SRP整合SSO的用户管理，如果SRP有自己的用户管理体系，那么需要和SSO同步数据，即如果SRP用户需要修改用户权限，那么必须向SSO进行同步，否则可能会导致SSO和SRP的数据不一致。
  
   * SRP应该在用户每次登录的时候做检查，判断用户的权限是否在SSO上被更改，如果被更改，则需要同步用户权限和SSO上保存的数据一致。具体参考下文：SRP从Token中获取用户角色。
  
   * 如果SRP没有用户管理系统，那么则完全依赖于SSO上的用户数据。所以在需要操作用户权限的时候，必须调用SSO的API完成。
  
   * 当SRP需要创建用户或修改用户权限时，需要调用如下接口。SSO会修改用户对于当前SRP的权限记录，并且如果用户不存在，那么SSO将会创建这个用户。这个接口调用需要tenant权限。

   ```
        PATCH /v2.0/users/{username}/scopes
        {
         "srpId": "<string>",
         "srpSecret": "<string>",
         "action": "<string>",
         "scopes": []
        }
        srpId: SSO provide
        srpSecret: SSO provide
        action: append or remove
        scopes:
        ["admin", ...]
        **Authentication
        EIToken in Bearer header or cookie
   ```

   * 接口参数 srpId 和 srpSecret为注册SRP时，SSO返回的参数。action表示执行的操作类型，添加或者移除。scopes 字段为需要赋予用户SRP的权限。

SRP的权限认证
---

### Token的response格式

   * SRP如果后端整合登录，需要SRP自己从SSO获取Token,返回格式如下。
   
   * expiresIn表示token的过期时间，另外accessToekn中解析出"exp"字段也对应Token的过期时间。Token的有效期默认为一个小时。
   
   * refreshToken可以刷新Token，refreshToken的有效期为24小时，在24小时内可以通过refreshToken重新获取一个Token,同时，旧的Token讲不能再使用。 
   
   * 如果要持续使用Token，建议在Token过期前10分钟在SSO refreshToken以获取新的Token,而不是每次调用/auth/native api获取新Token。参考：Token验证规则。                                                             
    
   * 如果需要在cookie中写入Token,应当写到环境变量中cf_api对应的domain中，分别写入EIToken,EIName在cookie中。

   ```   
    {
      "tokenType": "Bearer",
      "accessToken": "eyJhbGciOiJIUzUxMiJ9.xxxxxxxxxxxxx.T6Il7V8ZDODkdjRCO00FhA7VokwKqIqsa22o4ZONUdMql1z-sQe-xxx",
      "expiresIn": 1562058797,
      "refreshToken": "8e9b828f-5603-427c-aba5-xxxxxxxxxxxx"
    }
   ```

### Token验证规则

   * 1 如果用户携带Token，则直接执行第二步。如果用户使用账号密码登录，那么SRP应该先去SSO获取用户的Token,调用如下接口,此接口的返回格式参考：Token的response格式。

   ```
         POST /v2.0/auth/native
        {
         "username": "<string>",
         "password": "<string>"
        }
               
   ```

   * 2 SRP调用SSO的api，验证Token的有效性,使用如下接口。

   ```
        POST /v2.0/tokenvalidation
        
        {
            "token":"<string>"
        }
            
         此处token的值为 accessToken。
   ```

   * 3 SRP应该缓存每个Token的验证结果，直到到期，尽量不要每次调用SSO的API来验证它。
   
   * 4 Token刷新或重新登录，用户获取新令牌,刷新Token，使用如下接口。

   ```  
        POST /v2.0/token
        
        {
            "token":"<string>"
        }
              
         此处token的值为 refreshToken。
   ```

   * 5 SRP调用SSO的api，重新验证Token的有效性。  

### SRP从Token中获取用户角色

   *  1 解析Token,从中获取到用户的cfScopes，Token为jwt格式的Token,Token一共包含三段，用“.”分隔而开，SRP需要解除Token中的第二段，也就是携带用户信息的哪一段，使用base64解码即可。

   ```
    	str := strings.Split(accessToken, ".")
    	decodeBytes, errD := jose.Base64Decode([]byte(str[1]))     
    	
    	解析之后的Token格式如下：
    	{
        	"iss": "",
        	"iat": 1562131865,
        	"exp": 1562135465,
        	"userId": "",
        	"uaaId": "",
        	"creationTime": 1559277758000,
        	"lastModifiedTime": 1562131836000,
        	"username": "",
        	"firstName": "",
        	"lastName": "",
        	"country": "TW",
        	"role": "tenant",
        	"groups": [],
        	"cfScopes": [{
                         		"guid": "23f55516-7c9f-417c-9454-6031e8b621e6",
                         		"sso_role": "tenant",
                         		"spaces": ["*"]
                         	}],
        	"scopes": ["dashboard-1557046550862.Admin"],
        	"status": "active",
        	"origin": "SSO",
        	"overPadding": false,
        	"system": false,
        	"refreshToken": "7f656483-0744-4807-ab20-575e4b9e2ff0"
        }
   ```

   *  2 验证用户是否为当前org的tenant。如果为true，SRP应当赋予此用户当前SRP的最高权限，即cfScopes中的guid等于当前SRP的org_id并且sso_role等于tenant。                                                               
  
   *  3 如果步骤2验证失败，那么应从Token获取scopes，检查用户是否具有当前SRP的权限,即scopes中有当前SRPid对应的记录，“.”后面即为用户对于当前SRP的权限。
  
   *  4 特别的，如果用户是当前space的developer，并且在scopes中没有当前SRP的权限记录，那么他也应该被授予当前SRP的最低权限。
      
常用情景参考
---

### login

   *  1 用户输入帐户密码登录。
  
   *  2 SRP从环境变量中cf_api的值，根据cf_api获取当前SRP所在domain，并根据情况组合SSO的URL。

   ```  
        SSO URL 拼接规则：
        （1）WISE-PaaS org： https://portal-sso.${domain}
        （2）WISE-PaaS-Stage org：https://portal-sso-stage.${domain}
        （3）WISE-PaaS-Dev org：https://portal-sso-develop.${domain}
        （4）其他org：https://portal-sso.${domain}    
        
        例： cf_api的值为：https://api.wise-paas.com，且需要访问部署在WISE-PaaS org下的SSO，则SSO的URL为：https://portal-sso.wise-paas.com。
                
   ```

   *  3 调用SSO的/v2.0/auth/native api以获取Token。

   *  4 解析Token以验证用户权限。
  
   *  5 根据cf_api解析相应的domain，将EIToken和EIName返回到前端，并将cookie写入相应的域（EIToken->accesstoken，EIName->user'firstname）
      
      ```
         cf_api的值为：https://api.wise-paas.com，则domian为 .wise-paas.com或者wise-paas.com
      ```

### API 调用

   *  1 当在http请求的header中携带Token。 Authorization:Bearer {Token} 
  
   *  2 SRP需要验证Token是否有效以及令牌中的用户权限信息。验证过程参考：SRP从Token中获取用户角色
  
   *  3 使用basic auth（如果需要）调用API时，SRP使用用户帐户密码获取SSO Token，验证步骤与第二步相同。

### 创建或者修改SRP user

   *  1 如果SRP有自己的用户管理系统，当SRP需要创建或修改用户时，首先调用SSO的API修改，如果成功，再修改SRP自己的用户信息，这也可以保证数据的一致性。
   
   *  2 如果没有，直接调用SSO API。参考：设置用户对于当前SRP的权限

### 从SRP中删除用户

   *  1 调用SSO API。参考：设置用户对于当前SRP的权限,action 参数选择“remove”,删除用户对当前SRP的scopes记录。

参考demo以及api doc地址                                   
---

   *  sso整合demo后端gitlab地址：[sso-integration-demo-backend](http://advgitlab.eastasia.cloudapp.azure.com/EI-PaaS_TestProgram/sso-integration-demo-backend)
   
   *  sso整合demo前端gitlab地址：[sso-integration-demo-frontend](http://advgitlab.eastasia.cloudapp.azure.com/EI-PaaS_TestProgram/sso-integration-demo-frontend)
   
   *  sso apidoc 地址：[sso-apidoc](https://portal-sso.wise-paas.cn/apidoc/index.html)
