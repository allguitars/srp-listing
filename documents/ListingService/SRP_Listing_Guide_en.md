# SRP Listing Guide
This document is to give the details for the SRP listing process as a whole.

![](../uploads/images/ListingService/srp-listing-workflow.png)

<br>

## Prerequisites

Person in Charge: **SRP Development Team**

- Follow the [SRP Check List](../DevOps/SRP_Check.md) to do self-test and meet all **MUST** requirements. Provide the test result.
- Fill out all configurations for the SRP in [Apollo](http://portal-apollo-dev.eastasia.cloudapp.azure.com/).

- Follow the [pipeline development guideline](../DevOps/How_to_Write_a_Deployment_Pipeline.md) to develop the pipeline that can successfully deploys the SRP.
- Commit the pipeline in the `develop` branch and **tag** it as `v-x.x.x.x`. This pipeline should be in its own SRP pipeline project in GitLab.  

<br>

## Requesting Stage

Person in Charge: **SRP PM**

- Once the SRP passes the self-test, and the pipeline also passes the test, make a request in the **Support System**. Choose `SRP Listing Request` in the `Project` option menu.
- Fill out the following information in the request ticket:
    1. SRP Name
    2. SRP Version
    3. Release name of the configuration information in Apollo
    4. **Tag name** in the `develop` branch of the pipeline project
- The URL for the Support System depends on your WISE-PaaS domain:
    - [Support System @ HK](https://portal-support.wise-paas.com/)
    - [Support System @ Beijing](https://portal-support.wise-paas.cn/)

<br>

## Testing Stage

Person in Charge: **Listing System QA**

### Knowledge Transfer

- Check the Release Note and deployment architecture of the SRP to understand its functionalities and see if there is any need for knowledge transfer from the SRP dev team.
- If any knowledge transfer is required, the Listing System QA Team will give feedback to the SRP team immediately to arrange the knowledge transfer process.

### Testing the Pipeline

- Download the SRP package from the **development** section of the **Blob** and upload it to the corresponding **SRP directory** in the **production** section of the **Blob**.
- Copy the SRP configuration from the Apollo **DEV** section (development section used by the dev team) to the corresponding SRP in the **FAT** section (used by the QA team).
- In the SRP pipeline's GitLab project, compare the `develop` branch (created by the SRP dev team) and the `release` branch (created by the QA team). Modify the `release` branch accordingly, such as the *Blob URL* and *Apollo URL*.
- Run the deployment with Jenkins in the QA's test environment. Send back the result to the SRP dev team immediately and identify the root cause if the deployment fails.
- If the deployment is successful and this SRP is also running the listing process for the first time, follow the [SRP Check List Level-1](../DevOps/SRP_Check.md) to run the check. If the SRP does not meet the requirement, QA team will give feedback to the SRP dev team immediately. Once the SRP passes the SRP Level-1 check, QA team will just check if the SRP's [SSO](How_to_Integrate_with_SSO.md) login portal is working properly for the future release.
- Run an SRP **security scan** and practice for troubleshooting every quarter of the year. The result will be provided to the SRP team right away.

### Merging the Branch

- Once above tests all pass, run the following procedure:
  - Synchronize the SRP's configuration from the **FAT** section to the **PRO** section in Apollo.
  - In SRP pipeline's GitLab project, merge the `release` branch to the `master` branch.

<br>

## Listing the SRP to MarketPlace

Person in Charge: **Listing System PM**

- In **CodePipeline**, create a new Pipeline named as `pipeline_[SRPName]_Release` and link it up with the `master` branch of the SRP pipeline's GitLab project.
- Use the MarketPlace backend deployment API to deploy the SRP's corresponding version. Check if the [SSO](How_to_Integrate_with_SSO.md) log-in portal of the SRP is working properly and give feedback to he SRP team.
- Enter the business stage of the MarketPlace.

