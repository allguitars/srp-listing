
# How to Write a Deployment Pipeline for Your App

This document is to introduce you how to create your own deployment pipeline with the template pipeline. You will be able to deploy your app to the cloud in a really easy and fast way. There are a few steps for deploying the app through the pipeline as following.

## 1. PREPARE Stage

- Get the app configuration information from the Apollo.

    ```groovy
        dir('json'){  
            //通过api获取apollo配置信息到appInfo.json
            sh 'curl "http://srplisting-apollo-wise-paas-dev.ali.wise-paas.com.cn/slsystem/v1/srps/${namespace}" -k > appInfo.json'
        }
        //获取python脚本用于处理从api获取到的json数据，并返回
        dir('cf_operation'){
            retry(2){
                git credentialsId: "${git_credential}", url: 'http://advgitlab.eastasia.cloudapp.azure.com/SRE/pipeline_all_config.git'
            }
        }
        sh 'python3 ./cf_operation/parseJson.py ./json/appInfo.json'
        //将处理过的python脚本读取到APPS列表中
        if(fileExists("config.json")){
            APPS = readJSON file: "config.json"
        }else{
            error "Connot get apollo config"
        }
    ```

- Initialize the config information.
    ```groovy
        //将cf_org和cf_space转化为小写,在写manifest时使用
        def init(){
            cfOrgLow="${cf_org}".toLowerCase()
            cfSpaceLow="${cf_space}".toLowerCase()
            if(route_domain == ""){
                route_domain = "${cf_domain}"             
            }
            //获取要部署的app的个数
            appNums = APPS['param']['appNums']
        }
    ```

- Get the last version.
    ```groovy
        //如果在smokeTest时失败需要回滚到上一个版本，此处提前获取上一个版本
        getPreviousVersion()
    ```

- Get `org_if` and `space_id`
    ```groovy
        org_id=readFile('orgid.txt').trim()
        space_id=readFile('spaceid.txt').trim()
    ```

- Run **ELK** inspection
- Check if the app to be deployed exists.
    ```groovy
        checkAppExist("beforeDeploy")
    ```

## 2. DEPLOYMENT Stage

- Define the service to be bound with your app.
    ```groovy
        //根据app要绑定的服务，来定义app的服务
        def defineService(appNames)
    ```

- Set up the **manifest** file
    ```groovy
        def setManifest(app,appZipName,serOrg,serviceMogo,servicePost,serviceRabbit)
    ```

    - There are two steps for setting up the **manifest** file
        - First, retrieve the ZIP package from your Blob storage and unzip it.
            ```groovy
                appZipName="${app['sourceZipName']}_${app['version']}.zip"
                sh "python3 ../cf_operation/blobUploadDownload.py download ${containerName} ${app['sourceZipPath']}\\\\${appZipName}  ./${appZipName}"
                sh "unzip -o -d ./ ${appZipName}"
            ```

        - Second, modify your **manifest** file accordingly.
            ```groovy
                sh "cp manifest-production.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
                if(LICENSE=="true"){
                    sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['namePrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}" 
                }else{
                    sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['namePrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}" 
                }
            ```

- Deploy your app.
    ```groovy
        //确定是否包含influxdb
        if(app.containsKey('influxdb')){
           sh "cf bind-service ${app['name']}-${app['version']} ${app['influxdb_service_instance_name']}"
        }
        if(IfELK=="true"){
            sh "cf bind-service ${app['name']}-${app['version']} ELK"
        }
        if(sso_url==""){
            sh "cf set-env ${app['name']}-${app['version']} sso_url ''"
        
         }else{
            sh "cf set-env ${app['name']}-${app['version']} sso_url ${sso_url}"
        } 
        if(mp_url==""){
            sh "cf set-env ${app['name']}-${app['version']} mp_url ''"
        
         }else{
            sh "cf set-env ${app['name']}-${app['version']} mp_url ${mp_url}"
        }       
        //如果app_seq不为空，那么久按顺序启动         
        if(app["app_seq"] == null){
            sh "cf start ${app['name']}-${app['version']}"
        }
    ```

## 3. STARTING Stage

- Start your apps in order according to the `app_seq`.
    ```groovy
        for(int i=0;i<appNums;i++){
             "${k}").toString()
            sKey(appM)){
            eqMap.get("1")
            ".toString()
            eqMap(mmm)
            eqMap.get("${n}")
        
            fullAppName = startSeqMap.get("${n}")
            echo fullAppName
            sh "cf start ${fullAppName}"
        }
        k = k+1
        n = n+1
    ```

## 4. Check the Deployment Result

- Check if the app has been deployed successfully. The **Status** should be *started*.
    ```groovy
        for(int i = 0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS.containsKey(appM) && APPS[appM]['status']=="0"){
                checkedApp = APPS[appM]
                checkedStatus = checkAppExist(checkedApp)
                APPS[appM]['checkStatus'] = checkedStatus
                if(checkedAppStatus == "0"){
                    rollStatus = true
                }
            }
            m = m+1
        }
        echo "checkApp end .............."
    ```

## 5. Rollback

- If the **Status** shows failed or not existed, then execute the **rollback** process. Then the app that was just deployed will be deleted.
    ```groovy
        or(int i=0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS.containsKey(appM)==true && APPS[appM] != null){
                if(APPS[appM]['status'] == "0"){
                    dApp = APPS[appM]['name']
                    dVersion = APPS[appM]['version']
                    dAppName = "${dApp}-${dVersion}"
                    echo dAppName
                    sh "cf delete ${dAppName} -r -f"
                }
            }
            m = m+1
        }
        echo "deploy app failed,it's will roll back to last version"
        return
    ```

## 6. Set App's Hostname

- `app_host`: will be used URL binding.
    ```groovy
        appNamePrefix = APPS[appM]['namePrefix']
        if(cfOrgLow=="wise-paas" && cfSpaceLow=="srp"){
            app_host="${appNamePrefix}"
        }else if(cfOrgLow=="wise-paas-dev" && cfSpaceLow=="srp"){
            app_host="${appNamePrefix}"
        }
        else if(cfOrgLow=="wise-paas-stage" && cfSpaceLow=="srp"){
            app_host="${appNamePrefix}"
        }else{
            app_host="${appNamePrefix}"
        }
    ```

## 7. Blue-Green Deployment

- This process will delete the version prior than the last version. For example, there has been an app with version v-1.0.0 and another with version v-2.0.0 in the app space. Now, you are deploying the app with version v-3.0.0. The version v-1.0.0 will then be **deleted** if your new deployment is successful.
- The app with version v-2.0.0 will be also **stopped** if the new deployment is successful.
- The reason why we keep the version v-2.0.0 is that if the version v-3.0.0 does not pass the **Smoke Test**, then we should go back to version v-2.0.0.
- We call it a **Parallel** stage
    ```groovy
        parallel {
            stage("bluegreenswitch_app1"){
                steps{
                    script{
                        app00 = APPS["app0"]
                        bluegreenswitch(app00)
                    }
                }
            }
            stage('bluegreenswitch_app2'){
                steps{
                    script{
                        app01 = APPS['app1']
                        bluegreenswitch(app01)
                    }
                }
            }
            stage('bluegreenswitch_app3'){
                steps{
                    script{
                        app02 = APPS['app2']
                        bluegreenswitch(app02)
                    }
                }
            }
        }
    ```

## 8. Smoke Test

- Make a request to an API of the app. If the response status is 200, we will assume that this app is able to be visited.
    ```groovy
        for(int i=0;i<appNums;i++){
            appM = ("app" + "${m}").toString()
            if(APPS[appM].containsKey("${appM}")){
                if(APPS[appM]['isFronted'] == "1"){
                    smokeTest("${APPS[appM]['name']}-${APPS[appM]['version']}")
                }
            }
            m = m+1
        }
    ```
    
## 9. Exception Handling (Post mechanism)

- **Failure**: If any of the above stages have problems, then we will enter the code block of Failure. This code block should execute the following three steps:
    1. Delete the app that was just deployed. There is no deletion if the app was not deployed.
    2. Go back to the previous version. If the previous version was not stopped, then there is no need for this step.
    3. Report the error message.
- **Success**: Enter this code block if each of the above stages has been run properly.