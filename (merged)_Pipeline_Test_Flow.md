
# Pipeline Test Flow

This process will be run by the EnSaaS Listing Testing Team as soon as the SRP Dev Team creates a ticket for the SRP listing.

## 1. Required Components

### 1.1 GitLab

- GitLab Group: `WISE-PaaS_SRP-Pipeline`
- GitLab Project: Every SRP will have a project named after the **SRPName**, e.g., RMM.
- GitLab Branch: `release` branch, used for the test

### 1.2 Blob

- The testing team will have the connection string for the blob.

### 1.3 Jenkins

- Currently we are using the same [Jenkins portal](http://jenkins-dev.eastasia.cloudapp.azure.com/) for both the development and the test.
- In Jenkins, the name of the pipeline for testing will be `pipeline_[SRPName]_test_xxx`.

### 1.4 Apollo

- We are now using one [Apollo](http://portal-apollo.eastasia.cloudapp.azure.com/) for both the **test** section and the **production** section.
- The **test** section is mapped to the **FAT** environment in Apollo.

## 2. Pipeline Test Flow

1. Download the SRP package from the **development** section of the Blob and upload it to the corresponding **SRP directory** in the **production** section of the Blob.
2. Make a copy of the SRP config from the Apollo **development** section to the corresponding SRP in the Apollo **FAT** section.
3. In the SRP's GitLab project, compare the `develop` branch and the `release` branch and modify the `release` branch accordingly, such as the Blob URL and Apollo config center.
4. Run the test in the test environment in Jenkins and send back the result to the SRP dev team immediately.