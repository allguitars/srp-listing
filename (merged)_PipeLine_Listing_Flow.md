
# Pipeline Listing Flow

Once the EnSaaS Listing System team has run the test, and the result is pass, the SRP will be listed on to the Marketplace.

## **1. Required Components**

### **1.1 GitLab**

- GitLab Group: `WISE-PaaS_SRP-Pipeline`
- GitLab Project: Every SRP will have a project named after the **SRPName**, e.g., RMM
- GitLab Branch: `release` branch, used for the testing team to run the test
- GitLab Branch: `master` branch, the **production** branch

### **1.2 Blob**

- Will contain both the **test** and the **production** sections. The testing team will have the connection string for the blob.

### 1.3 Jenkins

- Unlike the test and development, there will be a dedicated **production** environment in our [Jenkins portal](http://jenkins-dev.eastasia.cloudapp.azure.com/).
- In Jenkins, the name of the pipeline for testing will be `pipeline_[SRPName]_Release`.

### 1.4 Apollo

- We are now using one [Apollo](http://portal-apollo.eastasia.cloudapp.azure.com/) for both the **test** section and the **production** section.
- The **production** section is mapped to the **PRO** environment in Apollo.

## 2. SRP Listing Flow

1. Copy the SRP configurations from the Apollo **FAT** to the corresponding Apollo **PRO**.
2. In the SRP's GitLab project, merge the `release` branch to the `master` branch and **tag** `v-x.x.x`.
3. In **CodePipeline**, create a new Pipeline named as `pipeline_[SRPName]_Release` and link it to the `master` branch of the SRP's GitLab project.
4. Listing process completed. Give feedback to the SRP dev team.