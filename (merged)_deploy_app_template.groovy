//初始化
def init(){
	cfOrgLow="${cf_org}".toLowerCase()
	cfSpaceLow="${cf_space}".toLowerCase()
	if(route_domain == ""){
		route_domain = "${cf_domain}"             
	}
	appNums = APPS['param']['appNums']
	appServiceNums = APPS['appServiceLenngth']
}
//检查app是否存在，checkedApp:app的名字，nameOfApp：app的prefix
def checkAppExist(check){
	if(check == "beforeDeploy"){
		m = 0
		for(int i=0;i<appNums;i++){
			appM = ("app" + "${m}").toString()
			if(APPS.containsKey(appM))
			{
				nameApp = APPS[appM]['name']
				versionApp =  APPS[appM]['version']
				echo "${nameApp} version: ${versionApp}"
				checkedAppName = "${nameApp}-${versionApp}"
				sh " ./cf_operation/check_app_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space} ${checkedAppName}"
				checkedAppStatus=readFile("checkapp.txt").trim() 
				APPS[appM]['status'] = checkedAppStatus
				echo APPS[appM]['status']
				if(checkedAppStatus=="1")
				{
					echo "${checkedAppName} exist!!!"
				}
				else
				{
					echo "${checkedAppName} doesn't exist!!!"
				}
				if(APPS[appM]['postgresql_service_group']!= "")
				{
					postgresql_service_group = APPS[appM]['postgresql_service_group']
					echo postgresql_service_group
				}
			}			
			m = m+1
		}
	}else{
		nameApp = check['name']
		versionOfApp = check['version']
		echo "${nameApp} version: ${versionOfApp}"
		checkedAppName = "${nameApp}-${versionOfApp}"
		sh "./cf_operation/check_app_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space} ${checkedAppName}"
		checkedAppStatus=readFile("checkapp.txt").trim() 
		echo checkedAppStatus
		if(checkedAppStatus=="1"){
			echo "${checkedAppName} exist!!!"
		}else{
			echo "${checkedAppName} doesn't exist!!!"
		}
	}
	
	return checkedAppStatus
}
def defineService(appNames){
	if(appNames.containsKey('mongodb'))
	{
	
		if (mongodb_service_info == "") 
		{
			appNames['mongodb_service_name'] = "mongodb"
			appNames['mongodb_service_plan'] = "Shared"
			appNames['mongodb_service_instance_name'] = "mongodb"
		} 
		else
		{
			mongodb_info_list = mongodb_service_info.split(':')
			appNames['mongodb_service_name'] = mongodb_info_list[0]
			appNames['mongodb_service_plan'] = mongodb_info_list[1]
			appNames['mongodb_service_instance_name'] = mongodb_info_list[2]
		}
	}
	if(appNames.containsKey('rabbitmq'))
	{
	
		if (rabbitmq_service_info == "") 
		{
			appNames['rabbitmq_service_name'] = "p-rabbitmq"
			appNames['rabbitmq_service_plan'] = "standard"
			appNames['rabbitmq_service_instance_name'] = "rabbitmq"
		}
		else
		{
			rabbitmq_info_list = rabbitmq_service_info.split(':')
			appNames['rabbitmq_service_name'] = rabbitmq_info_list[0]
			appNames['rabbitmq_service_plan'] = rabbitmq_info_list[1]
			appNames['rabbitmq_service_instance_name'] = rabbitmq_info_list[2]
		}
	}
	if(appNames.containsKey('postgresql_service_group'))
	{
	
		if (postgresql_service_info == "") 
		{
			appNames['postgresql_service_name'] = "postgresql"
			appNames['postgresql_service_plan'] = "Shared"
			appNames['postgresql_service_instance_name'] = "postgresql"
        }
		else
		{
			postgresql_info_list = postgresql_service_info.split(':')
			appNames['postgresql_service_name'] = postgresql_info_list[0]
			appNames['postgresql_service_plan'] = postgresql_info_list[1]
			appNames['postgresql_service_instance_name'] = postgresql_info_list[2]
		}
	}
	if(appNames.containsKey('influxdb')){
	
		if (influxdb_service_info == "") 
		{
			appNames['influxdb_service_name'] = "influxdb"
			appNames['influxdb_service_plan'] = "Shared"
			appNames['influxdb_service_instance_name'] = "influxdb"
		}
		else
		{
			influxdb_info_list = influxdb_service_info.split(':')
			appNames['influxdb_service_name'] = influxdb_info_list[0]
			appNames['influxdb_service_plan'] = influxdb_info_list[1]
			appNames['influxdb_service_instance_name'] = influxdb_info_list[2]
		}
	}
}
def setManifest(app,appZipName,serOrg,serviceMogo,servicePost,serviceRabbit,MANIFEST_DIR){
	echo "setManifest start........."
	dir(MANIFEST_DIR)
	{
		srpName = APPS['param']['srp_name']
		serOrg = "org_name:${cf_org},org_id:${org_id}"
		if(app.containsKey('mongodb')){
			serviceMogo = "${app['mongodb_service_name']}:${app['mongodb_service_instance_name']}"
		}else{	
			serviceMogo = null
		}
		if(app.containsKey('rabbitmq')){
			serviceRabbit = "${app['rabbitmq_service_name']}:${app['rabbitmq_service_instance_name']}"
		}else{
			serviceRabbit = null
		}
		if(app.containsKey('postgresql_service_group')){
			servicePost = "${app['postgresql_service_name']}:${app['postgresql_service_instance_name']}"
		}else{
			servicePost = null
		}
		if(app['name'] == "portal-OEE-Config" || app['name'] == "OEE-AlarmNotify-Worker")
		{
			appZipName="${app['name']}_${app['version']}"
			echo "${appZipName}"
			retry(3)
			{
				  timeout(5)
				  {
					  git credentialsId: "${git_credential}", url: "${app['sourceZipPath']}"
				  }
            }
			TagName="v-${app['version']}"
            sh "git checkout $TagName"
            echo "--------version--------"
            echo "$TagName"
            echo "${app['version']}"
			
			if(app['urlPrefix'] != ""){
			sh "cp manifest.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
			if(LICENSE=="true"){
				sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['urlPrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}"	
			}else{
				sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} ${route_domain} ${app['urlPrefix']} ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}"	
			}
			}else{
				sh "cp manifest.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
				if(LICENSE=="true"){
					sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}"	
				}else{                                                                                                                                               
					sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}"	
				}
			}
			sh "cat manifest-${cfOrgLow}-${cfSpaceLow}.yml"
		}	
		
		if(app['name'] == "OEEUtilizCal-dataworker" || app['name'] == "OEEUtilizSig-dataworker" || app['name'] == "OEEUtilizWO-dataworker" )
		{
			appZipName="${app['sourceZipName']}_${app['version']}.zip"
			echo "${appZipName}"
			oeecontainerName = ""
			blob_container_key=''
			retry(3)
			{
				timeout(5)
				{	
					//下载指定版本的zip包
					sh "python3 ../cf_operation/blobUploadDownload.py download ${oeecontainerName} ${appZipName}  ./${appZipName} ${blob_container_key}"
				}
			}
			//解压zip包
			sh "unzip -o -d ./ ${appZipName}"	
			sh "cp manifest.yml manifest-${cfOrgLow}-${cfSpaceLow}.yml"
			if(LICENSE=="true"){
				sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg},LICENSE:${space_id_md5}"	
			}else{                                                                                                                                               
				sh "python3 ../cf_operation/ModifyManifest.py manifest-${cfOrgLow}-${cfSpaceLow}.yml ${cfOrgLow} ${cfSpaceLow} null null ${app['version']} ${servicePost} ${serviceMogo} ${serviceRabbit} ${serOrg}"	
			}
			sh "cat manifest-${cfOrgLow}-${cfSpaceLow}.yml"
		}
	}
	echo "setManifest end........."
}
def deployApp(app,serviceMogo,servicePost,serviceRabbit,MANIFEST_DIR){
	if(app.containsKey('mongodb')){
		serviceMogo = "${app['mongodb_service_name']}:${app['mongodb_service_plan']}:${app['mongodb_service_instance_name']}"
	}else{	
		serviceMogo = null
	}
	if(app.containsKey('rabbitmq')){
		serviceRabbit = "${app['rabbitmq_service_name']}:${app['rabbitmq_service_plan']}:${app['rabbitmq_service_instance_name']}"
	}else{
		serviceRabbit = null
	}
	if(app.containsKey('postgresql_service_group')){
		servicePost = "${app['postgresql_service_name']}:${app['postgresql_service_plan']}:${app['postgresql_service_instance_name']}:${app['postgresql_service_group']}"
	}else{
		servicePost = null
	}
	retry(3){
		sh "./cf_operation/deploy_cf_service_nostart.sh api.${cf_domain} ${cf_username} ${cf_password} ${cf_org} ${cf_space} ${servicePost} ${serviceMogo} ${serviceRabbit} ${MANIFEST_DIR} manifest-${cfOrgLow}-${cfSpaceLow}.yml"
	}
	if(IfInfluxdb =="true" && app.containsKey('influxdb')){
	   sh "cf bind-service ${app['name']}-${app['version']} ${app['influxdb_service_instance_name']}"
	}
	if(IfELK=="true")
	{
		sh "cf bind-service ${app['name']}-${app['version']} ELK"
	}
	if(sso_url=="")
	{
		sh "cf set-env ${app['name']}-${app['version']} sso_url ''"
	}else
	{
		sh "cf set-env ${app['name']}-${app['version']} sso_url ${sso_url}"
	} 
	if(mp_url==""){
		sh "cf set-env ${app['name']}-${app['version']} mp_url ''"
	}else{
		sh "cf set-env ${app['name']}-${app['version']} mp_url ${mp_url}"
	}
	sh "cf start ${app['name']}-${app['version']}"
}
def Deploydependsrps(app)
{
	if(app['name'] == "Grafana"){
		build job: 'Pipeline-Grafana-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'StringParameterValue', name: 'PrivatePluginsURL', value: "${PrivatePluginsURL}"],
			[$class: 'StringParameterValue', name: 'logo', value: "${logo}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'DeployAllPlugins', value: "${DeployAllPlugins}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
	if(app['name'] == "SCADA")
	{
		build job: 'Pipeline-SCADA-test-Release', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'Deployversion', value: "${app['version']}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'StringParameterValue', name: 'mongodb_service_info', value: "${mongodb_service_info}"],
			[$class: 'StringParameterValue', name: 'postgresql_service_info', value: "${postgresql_service_info}"],
			[$class: 'StringParameterValue', name: 'rabbitmq_service_info', value: "${rabbitmq_service_info}"],
			[$class: 'StringParameterValue', name: 'influxdb_service_info', value: "${influxdb_service_info}"],
			[$class: 'StringParameterValue', name: 'sso_url', value: "${sso_url}"],
			[$class: 'StringParameterValue', name: 'mp_url', value: "${mp_url}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfELK', value: "${IfELK}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'LICENSE', value: "${LICENSE}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfInfluxdb', value: "${IfInfluxdb}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfSmokeTest', value: "${IfSmokeTest}".toBoolean()]]
	}
}
def bluegreenswitch(app)
{
	if(app['urlPrefix'] != ""){
		build job: 'pipeline_Blue_Green_Switch_t', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'StringParameterValue', name: 'app_name', value: "${app['name']}-${app['version']}"],
			[$class: 'StringParameterValue', name: 'app_host', value: "${app['apphost']}"]]
	}else{
		build job: 'pipeline_Blue_Green_Switch_t', parameters: [
			[$class: 'StringParameterValue', name: 'cf_domain', value: "${cf_domain}"],
			[$class: 'StringParameterValue', name: 'cf_username', value: "${cf_username}"],
			[$class: 'PasswordParameterValue', name: 'cf_password', value: "${cf_password}"],
			[$class: 'StringParameterValue', name: 'cf_org', value: "${cf_org}"],
			[$class: 'StringParameterValue', name: 'cf_space', value: "${cf_space}"],
			[$class: 'BooleanParameterValue', name: 'IfMapUrlDirectly', value: "${IfMapUrlDirectly}".toBoolean()],
			[$class: 'BooleanParameterValue', name: 'IfRemainBlueVersion', value: "${IfRemainBlueVersion}".toBoolean()],
			[$class: 'StringParameterValue', name: 'app_name', value: "${app['name']}-${app['version']}"],
			[$class: 'StringParameterValue', name: 'app_host', value: "NoRoute"]]
	}
}
//smoke Test,appName:要测试的app,在此场景中为：portal-scada
def smokeTest(appName){
	com = '\'BEGIN {FS=" "}; {print $6 $7}\''
	sh "cf login -u ${cf_username} -p ${cf_password} -o ${cf_org} -s ${cf_space} -a api.${cf_domain} --skip-ssl-validation;cf target -o ${cf_org};cf target -s ${cf_space};cf apps | grep ${appName} | awk ${com} >>urls.txt"	
	sh 'cat urls.txt' 
	url=readFile("urls.txt")split(',')
	for(int i=0;i<url.length;i++){
		u = url[i].trim()
		if (u != "")
		{
    		sh "curl -I -o /dev/null -s -w %{http_code} https://${u} >>ss.txt"
    		stateCodeWithVersion=readFile('ss.txt')
    		sh 'truncate -s 0 ss.txt'
    		if(stateCodeWithVersion != "200"){
    			error 'smoke test failed'
    		}else{
    			echo "smoke test success"
    		}
		}
	}
	sh 'truncate -s 0 urls.txt'
	sh 'cat urls.txt' 
}
pipeline{
	agent {
		kubernetes {
		  //cloud 'kubernetes'
		  label 'jenkins-node'
		  containerTemplate {
			name 'jenkins-node'
			image 'advimages.azurecr.io/jenkins-alpinenode:v4'
			ttyEnabled true
			// command 'cat'
		  }
		}
	}
	parameters{
		credentials(name:'git_credential',defaultValue:'',description: '')
		string(name:'Deployversion',defaultValue:'',description: '')		
		string(name:'cf_username',defaultValue:'',description: '')  
		password(name:'cf_password',defaultValue:'',description: '')  
		string(name:'cf_org',defaultValue:'',description: '')  
		string(name:'cf_space',defaultValue:'',description: '') 
		string(name:'cf_domain',defaultValue:'',description: '') 
		string(name:'mongodb_service_info',defaultValue:'',description:'')
		string(name:'postgresql_service_info',defaultValue:'',description:'')
		string(name:'rabbitmq_service_info',defaultValue:'',description:'')
		string(name:'influxdb_service_info',defaultValue:'',description:'')
		booleanParam(name:'IfSmokeTest',defaultValue:true,description:'')  //IfSmokeTest：是否进行smoketest
		booleanParam(name:'IfMapUrlDirectly',defaultValue:true,description:'')  //IfMapUrlDirectly:是否进行url映射
		booleanParam(name:'IfRemainBlueVersion',defaultValue:false,description:'')//IfRemainBlueVersion：是否停用上一个版本，删除以前的版本
		string(name:'sso_url',defaultValue:'',description:'')
		string(name:'mp_url',defaultValue:'',description:'')
		string(name:'logo',defaultValue:'',description:'')
		string(name:'DeployApps',defaultValue:'OEEUtilizCal-dataworker,portal-OEE-Config,OEEUtilizSig-dataworker,OEEUtilizWO-dataworker,OEE-AlarmNotify-Worker,Grafana,SCADA',description:'')
		booleanParam(name:'IfELK',defaultValue:false,description:'')
		booleanParam(name:'LICENSE',defaultValue:true,description:'')
		booleanParam(name:'DeployAllPlugins',defaultValue:false,description:'')
		booleanParam(name:'IfInfluxdb',defaultValue:false,description:'')
		string(name:'PrivatePluginsURL',defaultValue:'',description:'')
	}
	stages
	{
		stage('Prepare'){
			steps{
				script
				{
					echo "Prepare start"
					sh 'rm -rf *'
					//参数定义
					route_domain = ""
					app_host = ""
					APPS = ""
					org_id = ""
					space_id_md = ""
					checkStatus = ""
					rollStatus = "false"
					appNums = ""
					endFlag = 0
					app_prefix = ""
					app_name = ""
					//1.获取被解析apollo上的json 文件
					dir('json')
					{
						if(Deployversion == "")
						{
							sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/OEE" -k > appInfo.json'
						}
						else
						{
							sh 'curl "http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/OEE?${Deployversion}" -k > appInfo.json'
						}
					}
					dir('cf_operation'){
						retry(2){
							git credentialsId: "${git_credential}", url: 'http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/DevPublic.git'
						}
						sh 'chmod 770 *.sh'
					}
					sh 'python3 ./cf_operation/parseJson.py ./json/appInfo.json'
					if(fileExists("apollo.json")){
						APPS = readJSON file: "apollo.json"
					}else{
						error "Connot get apollo config"
					}
					
					//初始化
					init()
					//获取上一个版本
					sh "cf login -u $cf_username -p $cf_password -o $cf_org -s $cf_space -a api.$cf_domain --skip-ssl-validation;cf org $cf_org --guid>>orgid.txt;cf space $cf_space --guid>>spaceid.txt;"
					sh 'chmod 755 ~/.cf/config.json'
					//3.登录cf获取到orgId和spaceId
					org_id=readFile('orgid.txt').trim()
					space_id=readFile('spaceid.txt').trim()
					if(org_id=="FAILED"){
						error "Get org_id failed!!!"
					}
					if(space_id=="FAILED"){
						error "Get space_id failed!!!"
					}else{
						sh "echo -n ${space_id}|md5sum|cut -d ' ' -f1 > md5.txt"
						space_id_md5 = readFile('md5.txt').trim()
						echo "space_id_md5: ${space_id_md5}"
					}
					//获取要部署的App
					if (DeployApps == "") 
					{
						DeployApps = "OEEUtilizCal-dataworker,portal-OEE-Config,OEEUtilizSig-dataworker,OEEUtilizWO-dataworker,OEE-AlarmNotify-Worker,Grafana,SCADA"
					}
					//4.下载检查app是否存在的脚本
					dir('cf_operation') {
						if(IfELK=="true"){
							//执行文本检查：通过用户名，密码，域，org和space
							sh "../cf_operation/check_elk_exist.sh ${cf_username} ${cf_password} ${cf_domain} ${cf_org} ${cf_space}"
							//读取文件
							status=readFile("checkelk.txt").trim()
							echo status
							//状态值为0则创建日志
							if(status=="0"){
								sh "cf create-user-provided-service  ELK -l 'syslog://elk.eastasia.cloudapp.azure.com:5000'"
							}
						}
					}
					//检查app是否已经部署
					checkAppExist("beforeDeploy")
					//部署依赖的SRP
					echo "Prepare Finished"
				}
			}
		}
		//部署依赖的SRP Grafana   Scada
		stage('Deploydependsrps') 
		{
			parallel 
			{
				stage("app_services0")
				{
					when
					{
						expression
						{	
							h0 = "${APPS['app_services0']['name']}"
							echo h0
							return (APPS.containsKey("app_services0") && DeployApps.contains(h0))
						}
					}
					steps{
						script
						{
							app00 = APPS["app_services0"]
							Deploydependsrps(app00)
						}
					}
				}
				stage("app_services1")
				{
					when
					{
						expression
						{	
							h1 = "${APPS['app_services1']['name']}"
							return (APPS.containsKey("app_services1") && DeployApps.contains(h1))
						}
					}
					steps{
						script
						{
							app01 = APPS["app_services1"]
							Deploydependsrps(app01)
						}
					}
				}
			}
		}
		//部署OEE APP
		stage("deploy"){
			parallel{
				stage('app0'){
					when{
							
						expression
						{	
							h0 = "${APPS['app0']['name']}"
							return (APPS.containsKey("app0") && APPS['app0']['status']=="0" && DeployApps.contains(h0))
						}
					}
					steps{
						timeout(50){
							script{
								app00 = APPS['app0']
								defineService(app00)
								setManifest(app00,"app0Zipname","app0Org","mogo00","post00","rabbitmq00","app0")
								deployApp(app00,"mogo00","post00","rabbitmq00","app0")
								
							}
						}
					}
				}
				stage('app1'){
					when{
						expression
						{	h1 = "${APPS['app1']['name']}"
							return (APPS.containsKey("app1") && APPS['app1']['status']=="0" && DeployApps.contains(h1))
						}
					}
					steps{
						timeout(50){
							script{
								app01 = APPS['app1']
								defineService(app01)
								setManifest(app01,"app1Zipname","app1Org","mogo01","post01","rabbitmq01","app1")
								deployApp(app01,"mogo01","post01","rabbitmq01","app1")
							}
						}	
					}
				}
				stage('app2'){
					when{
						expression
						{	
							h2 = "${APPS['app2']['name']}"
							return (APPS.containsKey("app2") && APPS['app2']['status']=="0" && DeployApps.contains(h2))
						}
					}
					steps{
						timeout(50){
							script{
								app02 = APPS['app2']
								defineService(app02)
								setManifest(app02,"app2Zipname","app2Org","mogo02","post02","rabbitmq02","app2")
								deployApp(app02,"mogo02","post02","rabbitmq02","app2")
							}
						}	
					}
				}
				stage('app3'){
					when{
						expression
						{	
							h3 = "${APPS['app3']['name']}"
							return (APPS.containsKey("app3") && APPS['app3']['status']=="0" && DeployApps.contains(h3))
						}
					}
					steps{
						timeout(50){
							script{
								app03 = APPS['app3']
								defineService(app03)
								setManifest(app03,"app3Zipname","app3Org","mogo03","post03","rabbitmq03","app3")
								deployApp(app03,"mogo03","post03","rabbitmq03","app3")
							}
						}	
					}
				}
				stage('app4'){
					when{
						expression
						{	
							h4 = "${APPS['app4']['name']}"
							return (APPS.containsKey("app4") && APPS['app4']['status']=="0" && DeployApps.contains(h4))
						}
					}
					steps{
						timeout(50){
							script{
								app04 = APPS['app4']
								defineService(app04)
								setManifest(app04,"app4Zipname","app4Org","mogo04","post04","rabbitmq04","app4")
								deployApp(app04,"mogo04","post04","rabbitmq04","app4")
							}
						}	
					}
				}
			}
		}
		stage('checkApp'){
			steps{
				script
				{
					echo "check app start"
					m = 0;
					for(int i = 0;i<appNums;i++)
					{
						appM = ("app" + "${m}").toString()
						name = "${APPS[appM]['name']}"
						if(APPS.containsKey(appM) && APPS[appM]['status']=="0" && DeployApps.contains(name)){
							checkedApp = APPS[appM]
							checkedStatus = checkAppExist(checkedApp)
							APPS[appM]['checkStatus'] = checkedStatus
							if(checkedAppStatus == "0"){
								rollStatus = true
							}
						}
						m = m+1
					}
					echo "checkApp end .............."
				}
			}
		}
		//8.如果有app未部署成功，那么久回滚该所有app
		stage('rollBack'){
			when{
				expression{	
					return (rollStatus == true)
				}
			}
			steps{
				script{	
					m = 0;
						for(int i=0;i<appNums;i++){
							appM = ("app" + "${m}").toString()
							name = "${APPS[appM]['name']}"
							if(APPS.containsKey(appM)==true && APPS[appM] != null && DeployApps.contains(name))
							{
								if(APPS[appM]['status'] == "0")
								{
									dApp = APPS[appM]['name']
									dVersion = APPS[appM]['version']
									dAppName = "${dApp}-${dVersion}"
									echo dAppName
									sh "cf delete ${dAppName} -r -f"
								}
							}
							m = m+1
						}
						n = 0
					for(int j=0;j<appServiceNums;j++)
					{
						appN = ("app_services" + "${n}").toString()
						servicename = "${APPS[appN]['name']}"
						echo servicename
						serviceversion = "${APPS[appN]['version']}"
						echo serviceversion
						if(APPS.containsKey(appN) == true && DeployApps.contains(servicename))
						{
							dir('appservice')
							{
								if(serviceversion == "")
								{
									sh "curl -s 'http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/${servicename}' -k > appService.json"
								}
								else
								{
									sh "curl -s 'http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/${servicename}?${serviceversion}' -k > appService.json"
								}
							}
							sh 'python3 ./cf_operation/parseJson.py ./appservice/appService.json'
							if(fileExists("apollo.json"))
							{
								appservice = readJSON file: "apollo.json"
							}
							else
							{
								error "Connot get apollo config"
							}
							Nums = appservice['param']['appNums']
							nn = 0
							for(int k = 0;k<Nums;k++)
							{
								appNN = ("app" + "${nn}").toString()
								dApp = appservice[appNN]['name']
								dVersion = appservice[appNN]['version']
								dAppName = "${dApp}-${dVersion}"
								checkedApp = appservice[appNN]
								checkedStatus = checkAppExist(checkedApp)
								appservice[appNN]['checkStatus'] = checkedStatus
								if(checkedAppStatus == "1")
								{
									echo dAppName
									sh "cf delete ${dAppName} -r -f"
								}
								nn = nn+1
							}
						}
						n = n +1
					}
						endFlag = "1"
						echo "deploy app failed,it's will roll back to last version"
						error "Program execute failed"
				}
			}
		}
		stage('update-params'){
			steps{
				script{
					m = 0
					for(int i=0;i<appNums;i++){
						appM = ("app" + "${m}").toString()
						name = "${APPS[appM]['name']}"
						if(APPS[appM] == null || APPS[appM] == "" && DeployApps.contains(name))
						{
							continue
						}
						if(APPS[appM]['urlPrefix'] != "" && DeployApps.contains(name)){
							appNamePrefix = APPS[appM]['urlPrefix']
							if(appNamePrefix !=null && appNamePrefix!="")
							{
								Prefix = appNamePrefix.split(",")
								if(Prefix[0]!="" || Prefix[0]!=null)
								{app_host=""
									for(int j = 0;j<Prefix.length;j++)
									{
										if(j == Prefix.length-1)
										{
											app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"
										}
										else
										{
											app_host=app_host+"${Prefix[j]}-${cfOrgLow}-${cfSpaceLow}"+","
										}
									}
								}	
							}
							echo app_host
							APPS[appM]['apphost'] = app_host
							echo APPS[appM]['apphost']
						}
						m = m+1
					}
				}
			}
		}
		//12.冒烟测试
		stage('smokeTest')
		{
			when{
					expression{
						return (IfSmokeTest == "true")
					}
				}
			steps
			{
				script{
					m = 0
					for(int i=0;i<appNums;i++)
					{
						appM = ("app" + "${m}").toString()
						name = "${APPS[appM]['name']}"
						if(APPS.containsKey(appM) && DeployApps.contains(name))
						{
							if(APPS[appM]['urlPrefix'] != "")
							{
								smokeTest("${APPS[appM]['name']}-${APPS[appM]['version']}")
							}
						}
						m = m+1
					}
				}
			}
		}
		//10 蓝绿部署
		stage('bluegreenswitch_Stage') {
			parallel {
				stage("bluegreenswitch_app1"){
					when{
						expression
						{	
							return (APPS.containsKey("app0")&& APPS['app0']['status']=="0" && DeployApps.contains(h0))
						}
					}
					steps{
						script{
							app00 = APPS["app0"]
							bluegreenswitch(app00)
						}
					}
				}
				stage('bluegreenswitch_app2'){
					when{
						expression
						{
							return (APPS.containsKey("app1")&& APPS['app1']['status']=="0" && DeployApps.contains(h1))
						}
					}
					steps{
						script{
							app01 = APPS['app1']
							bluegreenswitch(app01)
						}
					}
				}
				stage('bluegreenswitch_app3'){
					when{
						expression
						{	
							return (APPS.containsKey("app2")&& APPS['app2']['status']=="0" && DeployApps.contains(h2))
						}
					}
					steps{
						script{
							app02 = APPS['app2']
							bluegreenswitch(app02)
						}
					}
				}
				stage('bluegreenswitch_app4'){
					when{
						expression{	
							return (APPS.containsKey("app3")&& APPS['app3']['status']=="0" && DeployApps.contains(h3))
						}
					}
					steps{
						script{
							app03 = APPS['app3']
							bluegreenswitch(app03)
						}
					}
				}
				stage('bluegreenswitch_app5'){
					when{
						expression{	
							return (APPS.containsKey("app4")&& APPS['app4']['status']=="0" && DeployApps.contains(h4))
						}
					}
					steps{
						script{
							app04 = APPS['app4']
							bluegreenswitch(app04)
						}
					}
				}
			}
		}
		stage("Import") 
		{
			steps
			{
				script
				{
					sh "cf create-service-key postgresql datasource-key -c '{\"groups\":[\"${postgresql_service_group}\"]}'"
					sh "cf service-key postgresql datasource-key>>serviceKey.txt"
					sh "ls"
					//	sh "cat serviceKey.txt"
					sh "tail -8 serviceKey.txt >datasource.json"
					sh "cat datasource.json"
					pgDatasouce = readJSON file: "datasource.json"				
					echo '================Read File==============='
					
					echo "PGHOST:${pgDatasouce['host']} "
					echo "PGusername:${pgDatasouce['username']} "
					dir("importgrafana")
					{
						retry(2)
						{
							git credentialsId: "${git_credential}", url: 'http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/DevPublic.git'
						}
					}
					dir("dashboardresource")
					{
						retry(2)
						{
							git branch: 'develop',credentialsId: "${git_credential}",url:"http://advgitlab.eastasia.cloudapp.azure.com/WISE-PaaS_SRP_Pipeline/OEE.git"
							sh "git checkout develop"  //下载develop分支的代码
							sh 'ls'
							dir("datasource")
							{
								sh 'ls'
								sh 'cat postgres.json'
								//修改datasource
								def sourcedata = readJSON file: "postgres.json"
								sourcedata['database'] = pgDatasouce['database']
								sourcedata['user'] = pgDatasouce['username']
								sourcedata['password'] = pgDatasouce['password']
								sourcedata.secureJsonData['password'] = pgDatasouce['password']
								sourcedata['url'] =  pgDatasouce['host'] 				
								echo "sourcedata:${sourcedata}"
								sh 'rm -rf *'
								echo '================output File==============='
								writeJSON file: 'postgres.json', json: sourcedata						
								sh 'ls'
								sh 'cat postgres.json'						
							}							
						}
					}	
					sh 'ls'
					dir("importgrafana")
					{
						sh 'ls'
						def dasboarUrl = "https://dashboard-${cfOrgLow}-${cfSpaceLow}.${cf_domain}"
						def srpframeReplace = "https://dashboard-ifactory-adviiot-ifactory-ifactory.wise-paas.com,https://dashboard-${cfOrgLow}-${cfSpaceLow}.${cf_domain}"
						sh "python3 ImportDashboard_1_2_0.py -a ${dasboarUrl} -t datasource -d ../dashboardresource/datasource"
						sh "python3 ImportDashboard_1_2_0.py -a ${dasboarUrl} -t dashboard -d ../dashboardresource/dashboard"													
						sh "python3 ImportDashboard_1_2_0.py -a ${dasboarUrl} -t srpframe -e ${srpframeReplace} -d ../dashboardresource/srpframe"
					}
				}
			}
		}
	}
	post 
	{ 			
		failure { 
			script {
				if(endFlag == 0){
						//1.回滚
					echo "rollBack start........."
					m = 0
					for(int i=0;i<appNums;i++){
						appM = ("app" + "${m}").toString()
						name = "${APPS[appM]['name']}"
						if(APPS.containsKey(appM)==true && APPS[appM] != null && DeployApps.contains(name)){
							if(APPS[appM]['status'] == "0"){
								dApp = APPS[appM]['name']
								dVersion = APPS[appM]['version']
								dAppName = "${dApp}-${dVersion}"
								echo dAppName
								sh "cf delete ${dAppName} -r -f"
							}
						}
						m = m+1
					}
					n = 0
					for(int j=0;j<appServiceNums;j++)
					{
						appN = ("app_services" + "${n}").toString()
						servicename = "${APPS[appN]['name']}"
						echo servicename
						serviceversion = "${APPS[appN]['version']}"
						echo serviceversion
						if(APPS.containsKey(appN) == true && DeployApps.contains(servicename))
						{
							dir('appservice')
							{
								if(serviceversion == "")
								{
									sh "curl -s 'http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/${servicename}' -k > appService.json"
								}
								else
								{
									sh "curl -s 'http://listingsystem-dev.eastasia.cloudapp.azure.com/listsystem/v1/srps/${servicename}?${serviceversion}' -k > appService.json"
								}
							}
							sh 'python3 ./cf_operation/parseJson.py ./appservice/appService.json'
							if(fileExists("apollo.json"))
							{
								appservice = readJSON file: "apollo.json"
							}
							else
							{
								error "Connot get apollo config"
							}
							Nums = appservice['param']['appNums']
							nn = 0
							for(int k = 0;k<Nums;k++)
							{
								appNN = ("app" + "${nn}").toString()
								dApp = appservice[appNN]['name']
								dVersion = appservice[appNN]['version']
								dAppName = "${dApp}-${dVersion}"
								checkedApp = appservice[appNN]
								checkedStatus = checkAppExist(checkedApp)
								appservice[appNN]['checkStatus'] = checkedStatus
								if(checkedAppStatus == "1")
								{
									echo dAppName
									sh "cf delete ${dAppName} -r -f"
								}
								nn = nn+1
							}
						}
						n = n +1
					}
					echo "rollBack end........."
					result = 'FAILURE'
					echo "something wrong,all operation rollBack"
				}
			}
				
		}
		success{
			script{
				echo "SUCCUSS"
			}
		}
	}
}